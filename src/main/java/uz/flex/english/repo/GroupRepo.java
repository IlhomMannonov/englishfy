package uz.flex.english.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.flex.english.entity.Group;
import uz.flex.english.payload.interfaseDto.GroupInterfaceDto;

import java.util.UUID;

public interface GroupRepo extends JpaRepository<Group, UUID> {
    @Query(value = "select cast(g.id as varchar), g.name, count(d) as members from groups g join direct d on g.id = d.group_id  join level l on g.level_id = l.id where l.id = :levelId group by  g.id, d.id, g.name " , nativeQuery = true)
    Page<GroupInterfaceDto> getAllByLevelId(UUID levelId, Pageable pageable);


}
