package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.flex.english.entity.LevelVocabularyAmount;

import java.util.List;
import java.util.UUID;

public interface LevelAmountRepo extends JpaRepository<LevelVocabularyAmount, UUID> {
    LevelVocabularyAmount getByActiveTrue();
}
