package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.flex.english.entity.CoinValue;
import uz.flex.english.entity.enums.CoinType;

public interface CoinValueRepo extends JpaRepository<CoinValue, Long> {
    CoinValue getCoinValueByCoinType(CoinType coinType);
}
