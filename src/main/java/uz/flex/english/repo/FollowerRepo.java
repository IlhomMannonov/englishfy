package uz.flex.english.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.flex.english.entity.Follower;
import uz.flex.english.entity.User;
import uz.flex.english.payload.interfaseDto.UserInterfaceDto;

import java.util.List;
import java.util.UUID;

public interface FollowerRepo extends JpaRepository<Follower, UUID> {

    boolean deleteByUserAndFollowerUser(User user, User followerUser);

    boolean existsByUserAndFollowerUser(User user, User followerUser);




}
