package uz.flex.english.repo;
import org.springframework.data.jpa.repository.JpaRepository;
import uz.flex.english.entity.Level;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
public interface LevelRepo extends JpaRepository<Level, UUID> {
    Level getLevelByOrdinalNumber(int ordinalNumber);
    List<Level>findAllByActiveIsTrue();
    Optional<Level> findByIdAndActiveIsTrue(UUID id);

}



