package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.flex.english.entity.Word;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface WordRepo extends JpaRepository<Word, UUID> {
    boolean existsAllByUnitId(UUID unit_id);
    boolean existsByWordUzAndWordEng(String wordUz, String wordEng);
    Optional<Word> getByWordEngAndWordUz(String wordEng, String wordUz);
    List<Word> getAllByUnitIdAndActiveTrue(UUID unit_id);
    @Query(value = "select id from word where word_uz=:wordUz and  word_eng=:wordEng",nativeQuery = true)
    UUID getIdByWordUzAndWordEng(String wordUz,String wordEng);

    @Query(value = "select w.* from  word w " +
            " join unit u on u.id = w.unit_id" +
            " where w.active = true and u.unit_number >=:fromNumber and u.unit_number <=:toNumber order by random() limit :wordLimit",nativeQuery = true)
    List<Word> getAllByUnitNumber(int fromNumber,int toNumber,int wordLimit);

    List<Word> getAllByActiveIsTrue();
}
