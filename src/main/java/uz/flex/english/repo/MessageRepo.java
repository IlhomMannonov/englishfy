package uz.flex.english.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.flex.english.entity.Message;
import uz.flex.english.payload.interfaseDto.MessageInterfaceDto;

import java.util.UUID;

public interface MessageRepo extends JpaRepository<Message, UUID> {
    @Query(value = "select cast(m.id as varchar), m.text, cast(u.id as varchar)  as userId,   u.name as name,  cast(m.message_id as varchar) messageId,  (select text from message where id = m.message_id) as replyText,case when m.user_id = :me then true else false end as isMine,  m.createdat from message m join direct d on m.direct_id = d.id   join users u on m.user_id = u.id where d.id = :directId and d.group_id is null order by m.createdat desc", nativeQuery = true)
    Page<MessageInterfaceDto> findAllByDirectChatId(UUID directId, UUID me, Pageable pageable);
    @Query(value = "select cast(m.id as varchar), m.text, cast(u.id as varchar) as userId, (select text from message where id = m.message_id) as replyText, case when m.user_id = :me then true else false end as isMine, m.createdat from message m join direct d on m.direct_id = d.id join users u on m.user_id = u.id where d.group_id = :groupId order by m.createdat desc", nativeQuery = true)
    Page<MessageInterfaceDto> findAllByGroupId(UUID groupId, UUID me, Pageable pageable);
}
