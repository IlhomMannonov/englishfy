package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.flex.english.entity.Unit;

import java.util.List;
import java.util.UUID;

public interface UnitRepo extends JpaRepository<Unit, UUID> {

    List<Unit> getAllByBookIdAndActiveIsTrue(UUID book_id);
    boolean existsById(UUID id);
    List<Unit> getAllByActiveIsTrue();
    boolean existsByNameAndBookId(String name, UUID book_id);
}
