package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.flex.english.entity.SolvedTense;
import uz.flex.english.entity.Tense;
import uz.flex.english.entity.User;
import uz.flex.english.payload.interfaseDto.MySolvedTensesDto;

import java.util.List;
import java.util.UUID;

public interface SolvedTenseRepo extends JpaRepository<SolvedTense, UUID> {
    boolean existsByTenseAndUser(Tense tense, User user);

    @Query(value = "select count(sat) from solved_tense st join solved_all_tense sat on st.id = sat.solved_tense_id where st.id = :solved_tense_id  and st.user_id = :user_id and sat.correct = true", nativeQuery = true)
    Integer getSolvedAmount(@Param(value = "solved_tense_id") UUID solved_tense_id, @Param(value = "user_id") UUID user_id);


    @Query(value = "with t as (select st.id ,t.name, u.username, count(sat) as amount, st.created_at  from solved_tense st join tense t   on t.id = st.tense_id  join users u on st.user_id = u.id join solved_all_tense sat on st.id = sat.solved_tense_id  where user_id = :user_id group by u.username, t.name, st.created_at, st.id) select cast(t.id as varchar ), t.name, t.username, t.amount, count(sat2) as correct, t.created_at from solved_tense s join solved_all_tense sat2 on s.id = sat2.solved_tense_id join t on t.id = sat2.solved_tense_id where correct = true group by t.created_at, t.username, t.name, t.id, t.amount", nativeQuery = true)
    List<MySolvedTensesDto> findMySolvedTensesByUserId(UUID user_id);


    SolvedTense getByUserIdAndId(UUID user_id, UUID tense_id);
}
