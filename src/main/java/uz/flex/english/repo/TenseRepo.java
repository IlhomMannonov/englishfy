package uz.flex.english.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.flex.english.entity.Tense;
import uz.flex.english.payload.interfaseDto.MySolvedTensesDto;
import uz.flex.english.payload.interfaseDto.TenseInterfaceDto;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TenseRepo extends JpaRepository<Tense, UUID> {
    List<Tense> findByTenseCategoryIdAndActiveIsTrue(UUID tenseCategory_id);

    @Query(value = "select cast (t.id as varchar ), name,  amount, active from tense t where t.tense_category_id =:tenseCategory_id", nativeQuery = true)
    List<TenseInterfaceDto> getAllByTenseCategoryId(UUID tenseCategory_id);

    @Query(value = "select cast (t.id as varchar ), name,  amount, (exists (select * from solved_tense st where st.tense_id=t.id and st.user_id=:user_id)  ) as success from tense t where t.tense_category_id=:category_id and t.active = true", nativeQuery = true)
    List<TenseInterfaceDto> getAllByTenseCategoryIdAndUserId(UUID category_id, UUID user_id);





}
