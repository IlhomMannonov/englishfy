package uz.flex.english.repo;


import org.springframework.data.jpa.repository.JpaRepository;
import uz.flex.english.entity.Views;


import java.util.List;
import java.util.UUID;

public interface ViewRepo extends JpaRepository<Views, UUID> {
    boolean existsByUserIdAndPostId(UUID user_id, UUID post_id);

    List<Views> getAllByPostId(UUID post_id);
}
