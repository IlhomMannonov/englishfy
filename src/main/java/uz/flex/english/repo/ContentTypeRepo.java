package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.flex.english.entity.ContentType;

public interface ContentTypeRepo extends JpaRepository<ContentType, Long> {
    boolean existsByName(String name);
}
