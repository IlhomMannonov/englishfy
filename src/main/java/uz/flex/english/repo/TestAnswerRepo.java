package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.flex.english.entity.TestAnswer;

import java.util.UUID;

public interface TestAnswerRepo extends JpaRepository<TestAnswer, UUID> {
}
