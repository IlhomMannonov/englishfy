package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.flex.english.entity.SolvedTest;
import uz.flex.english.entity.Test;
import uz.flex.english.entity.User;
import uz.flex.english.payload.interfaseDto.MySolvedTestDto;

import java.util.List;
import java.util.UUID;

public interface SolvedTestRepo extends JpaRepository<SolvedTest, UUID> {

    @Query(value = "select cast(st.id as varchar) ,t.name,u.username, st.created_at as time,(select count(*)from solved_test_answer sta where sta.solved_test_id = st.id) as amount, (select count(*) from solved_test_answer sta2 join test_answer ta on ta.id = sta2.test_answer_id where sta2.solved_test_id=st.id and ta.correct=true) as success from solved_test st join test t on t.id = st.test_id join users u on st.user_id = u.id where u.id=:user_id", nativeQuery = true)
    List<MySolvedTestDto> getByUserIdMySolvedTest(UUID user_id);

    @Query(value = "select case when count(s) = 1 then true else false end from solved_test s where user_id = :user_id  and test_id = :test_id", nativeQuery = true)
    Boolean existsByUserAndTest(UUID user_id, UUID test_id);
}
