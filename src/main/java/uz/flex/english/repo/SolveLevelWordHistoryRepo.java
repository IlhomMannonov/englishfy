package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.flex.english.entity.SolveLevelWordHistory;

import java.util.List;
import java.util.UUID;

public interface SolveLevelWordHistoryRepo extends JpaRepository<SolveLevelWordHistory, UUID> {
    List<SolveLevelWordHistory> getAllByUserIdAndLevelId(UUID user_id, UUID level_id);
    List<SolveLevelWordHistory> getAllByUserId(UUID user_id);
}
