package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.flex.english.entity.Book;

import java.util.List;
import java.util.UUID;

public interface BookRepo extends JpaRepository<Book, UUID> {
   boolean existsByName(String name);
    boolean existsById(UUID id);
    List<Book> getAllByActiveIsTrue();
}
