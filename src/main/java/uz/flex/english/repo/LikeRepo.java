package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.flex.english.entity.Comment;
import uz.flex.english.entity.Like;
import uz.flex.english.entity.Post;
import uz.flex.english.entity.User;

import java.util.UUID;

public interface LikeRepo extends JpaRepository<Like, UUID> {

    Like getByUserAndPost(User user, Post post);
    Like getByUserAndComment(User user, Comment comment);
}
