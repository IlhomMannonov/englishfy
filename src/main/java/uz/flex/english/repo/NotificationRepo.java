package uz.flex.english.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import uz.flex.english.entity.Notification;
import uz.flex.english.entity.User;

import java.util.UUID;

public interface NotificationRepo extends JpaRepository<Notification, UUID>{

    Page<Notification> findByUserId(UUID userId, Pageable pageable);

    void deleteByUserAndId(User user, UUID id);

    void deleteByUser(User user);

    Page<Notification> findByUserIdAndViewedFalse(UUID userId, Pageable pageable);
}
