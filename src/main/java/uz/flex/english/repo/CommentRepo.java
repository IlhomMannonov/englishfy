package uz.flex.english.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.flex.english.entity.Comment;
import uz.flex.english.payload.interfaseDto.CommentInterfaceDto;

import java.util.List;
import java.util.UUID;

public interface CommentRepo extends JpaRepository<Comment, UUID>{

    @Query(value = "select cast(c.id as varchar) as id, c.text, c.createdat, (select count(*) from likes where comment_id=c.id) as likes from comment c where c.post_id = :postId", nativeQuery = true)
    Page<CommentInterfaceDto> findByPostId(UUID postId, Pageable pageable);
}
