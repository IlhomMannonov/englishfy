package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.flex.english.entity.TenseCategory;

import java.util.List;
import java.util.UUID;

public interface TenseCategoryRepo extends JpaRepository<TenseCategory, UUID> {
    List<TenseCategory> findByTenseCategory_Id(UUID tense_id);
}
