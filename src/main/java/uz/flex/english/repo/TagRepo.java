package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.flex.english.entity.Tags;
import uz.flex.english.payload.interfaseDto.TagInterfaceDto;



import java.util.List;
import java.util.UUID;

public interface TagRepo extends JpaRepository<Tags, UUID> {
    @Query(value = "select cast(t.id as varchar ) as id, t.name from tags t join post_tags pt on pt.tags_id=t.id join post p on p.id = pt.post_id where p.id=:postId and t.active= true ",nativeQuery = true)
    List<TagInterfaceDto> getAllByPostId(UUID postId);
}
