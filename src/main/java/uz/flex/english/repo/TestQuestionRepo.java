package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.flex.english.entity.TestQuestion;

import java.util.UUID;

public interface TestQuestionRepo extends JpaRepository<TestQuestion, UUID> {

    @Query(value = "select count(t) from test_question t where test_id=:test_id", nativeQuery = true)
    Integer getTestQuestionCountByTestId(UUID test_id);



}
