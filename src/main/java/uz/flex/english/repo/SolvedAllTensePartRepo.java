package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.flex.english.entity.SolvedAllTensePart;

import java.util.UUID;

public interface SolvedAllTensePartRepo extends JpaRepository<SolvedAllTensePart, UUID> {
    @Query(value = "select input_word from solved_all_tense_part satp join solved_all_tense sat on satp.solved_all_tense_id = sat.id join solved_tense st on st.id = sat.solved_tense_id where st.id = :solved_tense_id and satp.all_tenses_part_id= :all_tenses_part_id ", nativeQuery = true)
    String findBySolvedAllTenseIdAndAllTensesPartId(UUID solved_tense_id, UUID all_tenses_part_id);
}
