package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.flex.english.entity.SolvedWordHistory;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface SolvedWordHistoryRepo extends JpaRepository<SolvedWordHistory, UUID> {
    Optional<List<SolvedWordHistory>> getAllByUnitIdAndUserId(UUID unit_id, UUID user_id);
    boolean existsByUserIdAndUnitId(UUID user_id, UUID unit_id);
    Optional<List<SolvedWordHistory>> getAllByUserId(UUID user_id);
}
