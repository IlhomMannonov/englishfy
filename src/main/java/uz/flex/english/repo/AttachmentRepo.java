package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.flex.english.entity.Attachment;
import uz.flex.english.payload.interfaseDto.AttachmentInterfaceDto;

import java.util.List;
import java.util.UUID;

public interface AttachmentRepo extends JpaRepository<Attachment, UUID> {


    @Query(value ="select cast (a.id as varchar )from attachment a join message_attachment_list mal on a.id = mal.attachment_list_id join message m on m.id = mal.message_id where m.id = :messageId", nativeQuery = true)
    List<UUID> findAllByMessageId(UUID messageId);
    @Query(value = "select cast(a.id as varchar )as id,a.content_type as contentType ,a.file_original_name as fileOriginalName,a.file_size as fileSize,a.name from attachment a join post_attachment_list pal on pal.attachment_list_id = a.id join post p on pal.post_id = p.id where p.id=:postId",nativeQuery = true)
    List<AttachmentInterfaceDto> getByPostId(UUID postId);

}
