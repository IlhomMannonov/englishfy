package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.flex.english.entity.SolvedTest;
import uz.flex.english.entity.SolvedTestAnswer;
import uz.flex.english.payload.interfaseDto.MySolvedTestAnswerDto;

import java.util.List;
import java.util.UUID;

public interface SolvedTestAnswerRepo extends JpaRepository<SolvedTestAnswer, UUID> {
    Integer countBySolvedTest_AndTestAnswerCorrect(SolvedTest solvedTest, boolean testAnswer_correct);

    @Query( value = "select cast(sta.id as varchar), tq.question, ta.text, ta.correct from solved_test_answer sta join test_question tq on tq.id = sta.test_question_id join test_answer ta on sta.test_answer_id = ta.id join solved_test st on sta.solved_test_id = st.id where sta.solved_test_id = :solved_test_id and st.user_id=:user_id", nativeQuery = true)
    List<MySolvedTestAnswerDto> getSolvedTestAnswerBySolvedTestAndUserId(UUID solved_test_id, UUID user_id);
}
