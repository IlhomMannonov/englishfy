package uz.flex.english.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.flex.english.entity.Post;
import uz.flex.english.payload.interfaseDto.PostInterfaceDto;

import java.util.List;
import java.util.UUID;

public interface PostRepo extends JpaRepository<Post, UUID> {

    List<Post> getAllByUserId(UUID user_id);
    Page<Post> findAllByUserId(UUID user_id,Pageable pageable);


    @Query(value = "select cast(p.id as varchar) as id,active, description, count(v.id) as views, count(l.id) as likes,count(c.id) as comments from post p join views v on v.post_id = p.id join likes l on l.post_id = p.id join comment c on c.post_id = p.id where p.id=:postId group by p.id",nativeQuery = true)
    PostInterfaceDto getByPostId(UUID postId);

    @Query(value = "select cast(p.id as varchar) as id,active, description, count(v.id) as views, count(l.id) as likes,count(c.id) as comments from post p join views v on v.post_id = p.id join likes l on l.post_id = p.id join comment c on c.post_id = p.id join users u on p.user_id = u.id where u.id=:userId group by p.id",nativeQuery = true)
    List<PostInterfaceDto> getAllByUser(UUID userId);

    @Query(value = "select cast(p.id as varchar) as id,active, description, count(v.id) as views, count(l.id) as likes,count(c.id) as comments from post p join views v on v.post_id = p.id join likes l on l.post_id = p.id join comment c on c.post_id = p.id group by p.id",nativeQuery = true)
    List<PostInterfaceDto> getAllPosts();

    @Query(value = "select cast(p.id as varchar) as id,active, description, count(v.id) as views, count(l.id) as likes,count(c.id) as comments from post p join views v on v.post_id = p.id join likes l on l.post_id = p.id join comment c on c.post_id = p.id join users u on p.user_id = u.id where u.id=:userId group by p.id",nativeQuery = true)
    Page<PostInterfaceDto> getAllPostsPageableByUserId(UUID userId,Pageable pageable);

    @Query(value = "select cast(p.id as varchar) as id, p.active ,description, count(v.id) as views, count(l.id) as likes,count(c.id) as comments from post p join views v on v.post_id = p.id join likes l on l.post_id = p.id join comment c on c.post_id = p.id join post_tags pt on pt.post_id = p.id join tags t on pt.tags_id = t.id join interest i on i.tags_id = t.id  join users u on u.id = i.user_id where u.id=:userId group by p.id ,p.createdat order by p.createdat desc",nativeQuery = true)
    Page<PostInterfaceDto> getInterestedPosts(UUID userId,Pageable pageable);
}
