package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.flex.english.entity.Interest;
import uz.flex.english.entity.Role;
import uz.flex.english.entity.enums.RoleName;

public interface RoleRepo extends JpaRepository<Role, Interest> {
    Role findByRoleName(RoleName roleName);
}