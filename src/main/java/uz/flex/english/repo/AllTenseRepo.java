package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.flex.english.entity.AllTense;

import java.util.UUID;

public interface AllTenseRepo extends JpaRepository<AllTense, UUID> {
}
