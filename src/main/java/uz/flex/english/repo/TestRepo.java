package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.flex.english.entity.Test;
import uz.flex.english.payload.interfaseDto.TestInterfaceDto;

import java.util.List;
import java.util.UUID;

public interface TestRepo extends JpaRepository<Test, UUID> {

    @Query(value = "select cast(t.id as varchar), t.name,t.coin_amount as coin, exists( select * from solved_test st where test_id = t.id and st.user_id = :user_id ) as processed from test t where level_id=:level_id", nativeQuery = true)
    List<TestInterfaceDto> getAllByUserIdAndLevelId(UUID user_id, UUID level_id);
}
