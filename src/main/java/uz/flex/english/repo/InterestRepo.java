package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import uz.flex.english.entity.Interest;
import org.springframework.transaction.annotation.Transactional;
import java.util.UUID;

public interface InterestRepo extends JpaRepository<Interest, UUID> {
    @Modifying
    @Transactional
    @Query(value = "delete from Interest where user_id=:userId and tags_id=:tagId and post_id=:postId", nativeQuery = true)
    void removeByUserIdAndTagsId(UUID userId, UUID tagId, UUID postId);

    Integer countByTagsIdAndUserId(UUID tags_id, UUID user_id);
    boolean existsByTagsIdAndUserId(UUID tags_id, UUID user_id);

    @Transactional
    @Modifying
    @Query(value = "delete from interest where id in (select i.id from interest i where user_id =:userId " +
            " order by createdat desc offset 20)",nativeQuery = true)
    void removeOldInterestsByUserId(UUID userId);

}
