package uz.flex.english.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.flex.english.entity.Direct;
import uz.flex.english.entity.User;
import uz.flex.english.payload.interfaseDto.DirectInterfaceDto;

import java.util.List;
import java.util.Set;
import java.util.UUID;

public interface DirectRepo extends JpaRepository<Direct, UUID> {
    boolean existsByFromUserIdAndToUserId(UUID fromUser_id, UUID toUser_id);
    boolean existsByFromUserAndGroupId(User fromUser, UUID group_id);
    Direct getByFromUserAndGroupId(User fromUser, UUID group_id);
    Direct getByFromUserIdAndToUserId(UUID fromUser, UUID toUser_id);


    @Query(value = "with t as (select d.id as direct_id, case when from_user_id = :me then (select id from users where id = to_user_id) else (select id from users where id = from_user_id) end as user_id  from direct d  where d.from_user_id = :me  or d.to_user_id = :me and d.group_id is null) select distinct cast(u.id as varchar), cast(t.direct_id as varchar) as directid,  name, m.text as lastmessage, m.createdat as date from t join message m  on m.id = (select m.id from message m where direct_id = t.direct_id order by m.createdat desc limit 1) join users u on u.id = t.user_id order by m.createdat desc limit  :size offset :page", nativeQuery = true)
    List<DirectInterfaceDto> findAllByMyNetwork(UUID me, int page, int size);




}
