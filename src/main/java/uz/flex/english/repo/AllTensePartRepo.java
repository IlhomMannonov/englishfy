package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.flex.english.entity.AllTensePart;

import java.util.UUID;

public interface AllTensePartRepo extends JpaRepository<AllTensePart, UUID> {
}
