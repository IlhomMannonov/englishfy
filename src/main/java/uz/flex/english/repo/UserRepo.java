package uz.flex.english.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.flex.english.entity.User;
import uz.flex.english.payload.interfaseDto.UserInterfaceDto;

import java.util.List;
import java.util.UUID;

public interface UserRepo extends JpaRepository<User, UUID> {

    User findByUsernameOrEmail(String username, String email);

    boolean existsByUsername(String username);

    boolean existsByEmail(String email);

    @Query(value = "select cast (u.id as varchar), u.name, u.last_name as lastname, u.username,  u.coin, u.bio, u.photo_url as photo,  (select count(post) from post  where post.user_id = u.id) posts, (select count(follower) from follower where follower.user_id = u.id) as following, (select count(follower) from follower  where follower.follower_user_id = u.id) as followers from users u join follower f2 on u.id = f2.user_id where f2.follower_user_id=:me", nativeQuery = true)
    List<UserInterfaceDto> findByMyFollowers(UUID me, Pageable pageable);

    @Query(value = "select cast (u.id as varchar ), u.name, u.last_name as lastname, u.username,  u.coin, u.bio, u.photo_url as photo, (select count(post) from post  where post.user_id = u.id) posts, (select count(follower) from follower where follower.user_id = u.id) as following, (select count(follower) from follower  where follower.follower_user_id = u.id) as followers from users u join follower f2 on u.id = f2.follower_user_id where f2.user_id = :me", nativeQuery = true)
    List<UserInterfaceDto> findByMyFollowing(UUID me, Pageable pageable);

    @Query(value = "select cast (u.id as varchar ), u.name, u.last_name as lastname, u.username, u.coin, u.bio,  u.photo_url as photo, (select count(p) from post p where p.user_id = u.id)posts, (select count(f) from follower f where f.user_id = u.id)as following, (select count(f) from follower f where f.follower_user_id = u.id) as followers, case when u.id= (select f.follower_user_id from follower f where f.follower_user_id =u.id  and f.user_id=:me) then true else false end as followed from users u where u.id=:user_id",nativeQuery = true)
    UserInterfaceDto findByUserId(UUID user_id, UUID me);

    @Query(value = "select cast (u.id as varchar ),  u.name, u.last_name,  u.username, u.bio,  u.coin, (select count(p) from post p where p.user_id = u.id) posts,(select count(f) from follower f where f.user_id = u.id) as following, (select count(f) from follower f where f.follower_user_id = u.id) as followers from users u join follower f on f.user_id = u.id  join follower f2 on f2.user_id = f.follower_user_id and f.user_id <> f2.user_id where f2.follower_user_id = :user_id   and u.id <> :user_id group by u.id order by count(f2.follower_user_id) desc  limit 40;", nativeQuery = true)
    List<UserInterfaceDto> findRecommendUser(UUID user_id);

    @Query(value = "select cast (u.id as varchar ),  u.name, u.last_name,  u.username, u.bio,  u.coin, (select count(p) from post p where p.user_id = u.id) posts,(select count(f) from follower f where f.user_id = u.id) as following, (select count(f) from follower f where f.follower_user_id = u.id) as followers from users u where u.username like concat('%',:username, '%') limit 30", nativeQuery = true)
    List<UserInterfaceDto> findByUsername(String username);

    @Query(value = "select cast (u.id as varchar )as id,  u.name, u.last_name,  u.username, u.bio,  u.coin, (select count(p) from post p where p.user_id = u.id) posts,(select count(f) from follower f where f.user_id = u.id) as following, (select count(f) from follower f where f.follower_user_id = u.id) as followers from users u join comment c on u.id = c.user_id where c.id=:commentId", nativeQuery = true)
    UserInterfaceDto findByCommentId(UUID commentId);
}
