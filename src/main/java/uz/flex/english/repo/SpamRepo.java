package uz.flex.english.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.flex.english.entity.Spam;

import java.util.UUID;

public interface SpamRepo extends JpaRepository<Spam, UUID> {
}
