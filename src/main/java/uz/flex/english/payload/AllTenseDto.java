package uz.flex.english.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AllTenseDto {
    private UUID id;
    private Integer number;
    private List<AllTensePartDto> tensePartDtoList;

}
