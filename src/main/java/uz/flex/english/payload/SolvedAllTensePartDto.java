package uz.flex.english.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SolvedAllTensePartDto {
    private UUID id;
    private boolean correct;
    private String inputWord;
}
