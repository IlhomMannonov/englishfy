package uz.flex.english.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TestQuestionDto {
    private UUID id;
    private String question;
    private boolean active;
    private List<TestAnswerDto> testAnswerDtoList;
}
