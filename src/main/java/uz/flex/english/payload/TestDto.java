package uz.flex.english.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.flex.english.entity.TestQuestion;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TestDto {
    private UUID id;
    private String name;
    private UUID levelId;
    private List<TestQuestionDto> testQuestionDtoList;
}
