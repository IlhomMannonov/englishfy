package uz.flex.english.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;


// this dto is only sending message
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MessageDto {
    private UUID id;
    private UUID userId;
    private UUID messageId;
    @NotNull
    private String text;
    private UUID directId;
    private Set<UUID> attachmentIds;
}
