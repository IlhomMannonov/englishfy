package uz.flex.english.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResToken {

    private String typeToken = "Bearer ";
    private String accessToken;

    public ResToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
