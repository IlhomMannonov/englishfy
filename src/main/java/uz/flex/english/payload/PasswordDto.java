package uz.flex.english.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PasswordDto {
    private String oldPassword;
    private String newPassword;
    private String prePassword;
}
