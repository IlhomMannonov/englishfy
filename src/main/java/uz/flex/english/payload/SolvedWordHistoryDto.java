package uz.flex.english.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.flex.english.entity.CoinValue;

import java.util.List;
import java.util.UUID;


@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SolvedWordHistoryDto {
    private UUID id;
    private UnitDto unitDto;
    private UserDto userDto;
    private CoinValue coinValue;
    private List<SolvedWordPartDto> wordPartDtoList;
}
