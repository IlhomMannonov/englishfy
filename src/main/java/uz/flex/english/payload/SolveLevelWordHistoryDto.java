package uz.flex.english.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.flex.english.entity.CoinValue;
import uz.flex.english.entity.Level;
import uz.flex.english.entity.SolvedLevelPart;
import uz.flex.english.entity.User;


import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SolveLevelWordHistoryDto {
    @NotNull
    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private UserDto userDto;
    @NotNull
    private LevelDto levelDto;
    @NotNull
    private List<SolveLevelPartDto> levelPartDtos;
}
