package uz.flex.english.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.flex.english.entity.TenseCategory;

import java.util.List;
import java.util.UUID;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TenseDto {
    private UUID id;
    private String name;
    private boolean active;
    private Integer amount;
    private TensesCategoryDto tensesCategoryDto;
    private List<AllTenseDto> allTenseDtoList;
}
