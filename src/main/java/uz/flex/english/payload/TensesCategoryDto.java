package uz.flex.english.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TensesCategoryDto {
    private UUID id;
    @NotNull
    private String name;

    private UUID levelId;

    private UUID tenseCategoryId;

    public TensesCategoryDto(UUID id, String name) {
        this.id = id;
        this.name = name;
    }
}
