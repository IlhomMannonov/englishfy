package uz.flex.english.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.aspectj.lang.annotation.DeclareAnnotation;
import uz.flex.english.entity.Level;

import java.util.UUID;


@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UnitDto {
       private UUID id;
       private String name;
       private BookDto bookDto;
       private Integer unitNumber;
       private boolean active;
}
