package uz.flex.english.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.flex.english.entity.Attachment;
import uz.flex.english.entity.Tags;
import uz.flex.english.entity.User;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostDto {
    private UUID id;
    private List<UUID> attachmentIdList;
    private String description;
    private UserDto userDto;
    private Set<TagDto> tagDtos;
    private boolean active = true;
}
