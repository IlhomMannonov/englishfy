package uz.flex.english.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DirectDto {
    private UUID id;
    private UserDto fromUser;
    private UserDto toUser;
    private UUID    groupId;
    private boolean active;
}
