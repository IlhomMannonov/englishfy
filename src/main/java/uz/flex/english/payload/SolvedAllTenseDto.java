package uz.flex.english.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.flex.english.entity.SolvedAllTensePart;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SolvedAllTenseDto {
    private UUID id;
    private boolean correct;
    private List<SolvedAllTensePartDto> solvedAllTensePartDtoList;
}
