package uz.flex.english.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.flex.english.entity.SolvedAllTense;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SolvedTenseDto {
    private UUID id;
    private TenseDto tenseDto;
    private List<SolvedAllTenseDto> solvedAllTenseDtoList;

}
