package uz.flex.english.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LevelAmountDto {
  private UUID id;
  private  int levelAmount;
  private  int wordAmount;
  private  boolean active;
}
