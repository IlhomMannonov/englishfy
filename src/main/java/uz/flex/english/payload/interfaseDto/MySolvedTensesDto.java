package uz.flex.english.payload.interfaseDto;

import java.sql.Timestamp;
import java.util.UUID;

public interface MySolvedTensesDto {
    UUID getId();
    String getName();
    String getUsername();
    Integer getAmount();
    Integer getCorrect();
    Timestamp getCreateat();

}
