package uz.flex.english.payload.interfaseDto;


import org.springframework.beans.factory.annotation.Value;

import javax.persistence.PrePersist;
import java.util.List;
import java.util.UUID;

public interface PostInterfaceDto {

    UUID getId();
    String getDescription();
    Long getViews();
    Long getLikes();
    Long getComments();

    @PrePersist
    @Value("#{@attachmentRepo.getByPostId(target.id)}")
    List<AttachmentInterfaceDto> getAttachment();

    @PrePersist
    @Value("#{@userRepo.getByPostId(target.id)}")
    UserInterfaceDto getUser();

    @Value("#{@tagRepo.getAllByPostId(target.id)}")
    List<TagInterfaceDto> getTags();
    boolean isActive();
}
