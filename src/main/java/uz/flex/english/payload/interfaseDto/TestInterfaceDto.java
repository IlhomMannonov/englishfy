package uz.flex.english.payload.interfaseDto;

import java.util.UUID;

public interface TestInterfaceDto {
    UUID getId();
    String getName();
    Integer getCoin();
    Boolean getProcessed();
}
