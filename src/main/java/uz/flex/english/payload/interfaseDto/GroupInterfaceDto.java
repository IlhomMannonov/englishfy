package uz.flex.english.payload.interfaseDto;

import java.util.UUID;

public interface GroupInterfaceDto {
    UUID getId();

    UUID getDirectId();

    String getName();

    Long getMembers();
}
