package uz.flex.english.payload.interfaseDto;

import java.util.UUID;

public interface TenseInterfaceDto {
    UUID getId();

    String getName();

    Integer getAmount();

    Boolean getActive();

    Boolean getSuccess();


}
