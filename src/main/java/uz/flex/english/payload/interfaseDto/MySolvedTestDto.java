package uz.flex.english.payload.interfaseDto;

import io.swagger.models.auth.In;

import java.sql.Timestamp;
import java.util.UUID;

public interface MySolvedTestDto {

    UUID getId();

    String getName();

    String getUsername();

    Timestamp getTime();

    Integer getAmount();

    Integer getSuccess();
}
