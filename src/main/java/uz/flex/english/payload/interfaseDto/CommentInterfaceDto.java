package uz.flex.english.payload.interfaseDto;

import org.springframework.beans.factory.annotation.Value;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.UUID;

public interface CommentInterfaceDto {
    UUID getId();


    @Value("#{@userRepo.findByCommentId(target.id)}")
    UserInterfaceDto getUser();

    String getText();

    Long getLikes();

    Timestamp getCreatedAt();
}
