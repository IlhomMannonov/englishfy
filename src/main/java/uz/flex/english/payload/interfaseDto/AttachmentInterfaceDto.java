package uz.flex.english.payload.interfaseDto;

import java.util.UUID;

public interface AttachmentInterfaceDto {
    UUID getId();
    String getContentType();
    String getFileOriginalName();
    Long getFileSize();
    String getName();
}
