package uz.flex.english.payload.interfaseDto;

import java.util.UUID;

public interface TagInterfaceDto {
    UUID getId();
    String getName();
}
