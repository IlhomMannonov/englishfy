package uz.flex.english.payload.interfaseDto;

import java.util.UUID;

public interface MySolvedTestAnswerDto {
    UUID getId();

    String getQuestion();

    String getText();

    Boolean getCorrect();

}
