package uz.flex.english.payload.interfaseDto;

import java.util.UUID;

public interface UserInterfaceDto {

    UUID getId();

    String getName();

    String getLastname();

    Long getPosts();

    Long getFollowers();

    Long getFollowing();

    String getPhoto();

    String getBio();

    Boolean getFollowed();

    String getUsername();




}
