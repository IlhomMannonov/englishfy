package uz.flex.english.payload.interfaseDto;


import java.sql.Timestamp;
import java.util.UUID;

public interface DirectInterfaceDto {

    UUID getId();

    UUID getDirectid();

    String getName();

    String getLastMessage();

    Timestamp getDate();

}
