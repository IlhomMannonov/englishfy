package uz.flex.english.payload.interfaseDto;

import org.springframework.beans.factory.annotation.Value;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

public interface MessageInterfaceDto {
    UUID getId();
    String getText();
    String getName();
    UUID getUserId();
    UUID getMessageId();
    String getReplyText();
    Boolean getIsMine();
    Timestamp getCreatedat();
    @Value("#{@attachmentRepo.findAllByMessageId(target.id)}")
    List<UUID> getAttachmentList();



}
