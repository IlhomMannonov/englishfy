package uz.flex.english.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto {
    private UUID id;
    private Timestamp createdAt;
    private String name;
    private String phoneNumber;
    private String userName;
    private String lastName;
    private String email;
    private String photoUrl;
    private String bio;
    private boolean enable;
    private long coins;

    public UserDto(UUID id, String name, String userName,String lastName) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.userName = userName;
    }
}
