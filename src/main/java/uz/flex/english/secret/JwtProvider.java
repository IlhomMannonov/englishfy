package uz.flex.english.secret;

import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import uz.flex.english.entity.Role;
import uz.flex.english.entity.User;
import uz.flex.english.repo.UserRepo;

import java.util.*;

@Component
public class JwtProvider {

    @Value("${jwt.secretKey}")
    private String key;
    @Value("${jwt.expireDateInMilliSecund}")
    private Long expireDateMiliSecund;

    @Autowired
    UserRepo userRepo;

    public String generateToken(User user) {
        Date issueDate = new Date();
        Date expireDate = new Date(issueDate.getTime()+ expireDateMiliSecund);
        return Jwts
                .builder()
                .setSubject(user.getId().toString())
                .setIssuedAt(issueDate)
                .claim("roles", user.getRoles())
                .setExpiration(expireDate)
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();
    }
    public boolean validateToken(String token) {
        try {
            Jwts
                    .parser()
                    .setSigningKey(key)
                    .parseClaimsJws(token);
            return true;
        } catch (
                ExpiredJwtException e) {
            System.err.println("Muddati o'tgan");
            Date expiration = Jwts.parser()
                    .setSigningKey(key)
                    .parseClaimsJws(token)
                    .getBody()
                    .getExpiration();
        } catch (
                MalformedJwtException malformedJwtException) {
            System.err.println("Buzilgan token");
        } catch (
                SignatureException s) {
            System.err.println("Kalit so'z xato");
        } catch (
                UnsupportedJwtException unsupportedJwtException) {
            System.err.println("Qo'llanilmagan token");
        } catch (IllegalArgumentException ex) {
            System.err.println("Bo'sh token");
        }
        return false;
    }

    public User getUserFromToken(String token) {
        String userId = Jwts.parser()
                .setSigningKey(key)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        final Optional<User> userOptional = userRepo.findById(UUID.fromString(userId));
        return userOptional.orElseGet(null);
    }
}
