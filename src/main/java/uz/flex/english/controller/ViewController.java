package uz.flex.english.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.ViewDto;
import uz.flex.english.servise.ViewService;

import java.util.UUID;

@RestController
@RequestMapping("/view")
public class ViewController {
    @Autowired
    ViewService viewService;

    @PostMapping("/save")
    public HttpEntity<?> save(@RequestBody ViewDto viewDto){
        ApiResponse response = viewService.save(viewDto);
        return ResponseEntity.status(response.isSuccess()?201:409).body(response);
    }

    @GetMapping("/byPostId{id}")
    public HttpEntity<?> getByPost(@PathVariable UUID id){
        ApiResponse response = viewService.getByPost(id);
        return ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }


}
