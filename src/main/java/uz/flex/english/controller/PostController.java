package uz.flex.english.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.flex.english.entity.User;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.PostDto;
import uz.flex.english.secret.CurrentUser;
import uz.flex.english.servise.PostService;
import uz.flex.english.utills.AppConstants;

import java.util.UUID;

@RestController
@RequestMapping("/api/post")
public class PostController {

    @Autowired
    PostService postService;

    @PostMapping("/save")
    public HttpEntity<?> add(@RequestBody PostDto postDto, @CurrentUser User user) {
        ApiResponse response = postService.save(postDto, user);
        return ResponseEntity.status(response.isSuccess() ? 201 : 409).body(response);
    }

    @GetMapping("/all")
    public HttpEntity<?> getAll(@RequestParam(required = false) UUID id) {
        ApiResponse response = postService.getAll(id);
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }

    @GetMapping("/byUser{id}")
    public HttpEntity<?> getAll(@PathVariable UUID id, @RequestParam(required = false) boolean all, @RequestParam(required = false,defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page, @RequestParam(required = false,defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {
        ApiResponse response = postService.getUser(id, all, page, size);
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }


    @GetMapping("/pageableUserInterest{id}")
    public HttpEntity<?>getPageable(@PathVariable UUID id,@RequestParam(defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page, @RequestParam(defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size){
        ApiResponse response = postService.getPageable(id, size, page);
      return ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }

}
