package uz.flex.english.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.flex.english.entity.User;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.SpamDto;
import uz.flex.english.secret.CurrentUser;
import uz.flex.english.servise.SpamService;

@RestController
@RequestMapping("/api/spam")
public class SpamController {
    @Autowired
    private SpamService spamService;

    @PostMapping("/send")
    public HttpEntity<?> sendSpam(@CurrentUser User user, @RequestBody SpamDto spamDto) {

        ApiResponse apiResponse = spamService.sendSpam(user, spamDto);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }
}
