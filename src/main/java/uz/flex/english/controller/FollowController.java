package uz.flex.english.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.flex.english.entity.User;
import uz.flex.english.exceptions.PageSizeException;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.secret.CurrentUser;
import uz.flex.english.servise.FollowService;
import uz.flex.english.utills.AppConstants;

import java.util.UUID;

@RestController
@RequestMapping("/api/follow")
public class FollowController {

    @Autowired
    private FollowService followService;


    @PostMapping("/follow")
    public HttpEntity<?> follow(@CurrentUser User user,@RequestParam UUID userId) {
       ApiResponse apiResponse = followService.follow(user, userId);
       return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    @DeleteMapping("/unfollow")
    public HttpEntity<?> unfollow(@CurrentUser User user, UUID userId) {
        ApiResponse apiResponse = followService.unfollow(user, userId);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

    @GetMapping("/following")
    public HttpEntity<?> getFollowing(
            @RequestParam("userId") UUID userId,
            @RequestParam(defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size
    ) throws PageSizeException {
        return ResponseEntity.ok(followService.getFollowing(userId,page,size));
    }

    @GetMapping("/followers")
    public HttpEntity<?> getFollowers(
            @RequestParam("userId") UUID userId,
            @RequestParam(defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size
    ) throws PageSizeException {
        return ResponseEntity.ok(followService.getFollowers(userId,page,size));
    }
}

