package uz.flex.english.controller;

import org.hibernate.QueryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.flex.english.entity.User;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.SolvedWordPartDto;
import uz.flex.english.payload.WordDto;
import uz.flex.english.secret.CurrentUser;
import uz.flex.english.servise.WordService;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/word")
public class WordController {

    @Autowired
    WordService wordService;

    @GetMapping("/all")
    public HttpEntity<?> getAll(@RequestParam(required = false) UUID id){
        ApiResponse response = wordService.getAllOrById(id);
        return ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }

    @DeleteMapping("/delete/{id}")
    public HttpEntity<?> delete(@PathVariable UUID id){
        ApiResponse response = wordService.delete(id);
       return ResponseEntity.status(response.isSuccess()?202:409).body(response);
    }

    @GetMapping("/getByLevel{id}")
    public HttpEntity<?> getByLevel(@PathVariable UUID id){
        ApiResponse response = wordService.getLevelWords(id);
        return ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }


    @PostMapping("/save")
    public HttpEntity<?> save(@RequestBody List<WordDto> wordDtoList){
        try {
            ApiResponse  response = wordService.save(wordDtoList);
            return ResponseEntity.status(response.isSuccess() ? 201 : 409).body(response);
        }
        catch (Exception e){
            return ResponseEntity.status(409).body("You entered duplicate words!");
        }
    }

    @PutMapping("/edit")
    public HttpEntity<?> edit(@RequestBody List<WordDto> dtoList){
        ApiResponse response = wordService.edit(dtoList);
        return ResponseEntity.status(response.isSuccess()?201:409).body(response);
    }

    @GetMapping("/byUnitId/{id}")
    public HttpEntity<?> getByUnitId(@PathVariable UUID id){
        ApiResponse response = wordService.getByUnitId(id);
        return ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }

}
