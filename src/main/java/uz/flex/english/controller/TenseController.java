package uz.flex.english.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.flex.english.entity.User;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.SolvedTenseDto;
import uz.flex.english.payload.TenseDto;
import uz.flex.english.secret.CurrentUser;
import uz.flex.english.servise.TenseService;

import java.util.UUID;

@RestController
@RequestMapping("/api/tenses")
public class TenseController {

    @Autowired
    TenseService tenseService;


    @PostMapping("/saveOrEdit")
    @ApiOperation("edit qilish uchun tense id bolishi shart, edit qilish davomida yangi part va savol qoshish mumkin")
    public HttpEntity<?> save(@RequestBody TenseDto tenseDto) {
        ApiResponse apiResponse = tenseService.saveOrEdit(tenseDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    //faqat activlarni get qiladi
    @GetMapping("/get/{id}")
    public HttpEntity<?> get(@PathVariable UUID id) {
        ApiResponse apiResponse = tenseService.get(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    //faqat ishlash uchun kerak boladi, userlar uchun deactivelar korinmaydi
    @GetMapping("/getByCategoryId/{id}")
    public HttpEntity<?> getByCategoryId(@PathVariable UUID id, @CurrentUser User user) {
        ApiResponse apiResponse = tenseService.getByCategoryId(id, user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    //admin hamma tense larni tense_catecory_id orqali korishi mumkin
    @GetMapping("/getTenseByTenseCategoryForAdmin/{id}")
    public HttpEntity<?>getAllByTenseCategoryId(@PathVariable UUID id){
        ApiResponse apiResponse = tenseService.getAllByTenseCategoryId(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }


    // toplamni ochirish
    @DeleteMapping("/deleteTense/{id}")
    public HttpEntity<?> deleteTense(@PathVariable UUID id) {
        ApiResponse apiResponse = tenseService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 203 : 409).body(apiResponse);
    }

    //bitta savolni o'chirish
    @DeleteMapping("/deleteAllTense/{id}")
    public HttpEntity<?>deleteAllTense(@PathVariable UUID id){
        ApiResponse apiResponse = tenseService.deleteAllTense(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 203 : 409).body(apiResponse);
    }

    //savol partini ochirish
    @DeleteMapping("/deleteAllTensePart/{id}")
    public HttpEntity<?>deleteAllTensePart(@PathVariable UUID id){
        ApiResponse apiResponse = tenseService.deleteAllTensePart(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 203 : 409).body(apiResponse);
    }


    @PostMapping("/exam")
    public HttpEntity<?> exam(@RequestBody SolvedTenseDto solvedTenseDto, @CurrentUser User user) {
        ApiResponse apiResponse = tenseService.exam(solvedTenseDto, user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }


}
