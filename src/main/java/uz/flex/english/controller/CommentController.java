package uz.flex.english.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.flex.english.entity.User;
import uz.flex.english.exceptions.PageSizeException;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.CommentDto;
import uz.flex.english.repo.UserRepo;
import uz.flex.english.secret.CurrentUser;
import uz.flex.english.servise.CommentService;
import uz.flex.english.utills.AppConstants;

import java.util.UUID;

@RestController
@RequestMapping("/api/comment")
public class CommentController {
    @Autowired
    private CommentService commentService;

    @ApiOperation(value = "add comment any user for Post ")
    @PostMapping("/add")
    public HttpEntity<?> addComment(@CurrentUser User user, @RequestBody CommentDto commentDto) {
        ApiResponse apiResponse = commentService.addComment(user, commentDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @ApiOperation(value = "delete comment only add by user")
    @DeleteMapping("/delete/{id}")
    public HttpEntity<?> deleteComment(@CurrentUser User user, @PathVariable UUID id) {
        ApiResponse apiResponse = commentService.deleteComment(user, id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/get/{id}")
    public HttpEntity<?>getAllCommentByPostId(
            @PathVariable UUID id,
            @RequestParam(required = false, defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(required = false, defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size
    ) throws PageSizeException {
        ApiResponse apiResponse = commentService.getAllCommentByPostId(id, page, size);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);

    }

}
