package uz.flex.english.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.UnitDto;
import uz.flex.english.servise.UnitService;

import java.util.UUID;

@RestController
@RequestMapping("/unit")
public class UnitController {

    @Autowired
    UnitService unitService;

    @GetMapping("/byBookId/{id}")
    public HttpEntity<?> getAllByBookId(@PathVariable UUID id){
        ApiResponse res = unitService.getByBookId(id);
      return ResponseEntity.status(res.isSuccess()?200:409).body(res);
    }

    @GetMapping("/all/{id}")
    public HttpEntity<?> getAllOrById(@RequestParam(required = false) UUID id){
        ApiResponse response = unitService.getAllOrById(id);
        return ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }

    @PostMapping("/saveOrEdit")
    public HttpEntity<?> saveOrEdit(@RequestBody UnitDto unitDto){
        ApiResponse response = unitService.saveOrEdit(unitDto);
      return ResponseEntity.status(response.isSuccess()?201:409).body(response);
    }


    @DeleteMapping("/deleteById/{id}")
    public HttpEntity<?> delete(@PathVariable UUID id){
        ApiResponse response = unitService.delete(id);
        return  ResponseEntity.status(response.isSuccess()?202:409).body(response);
    }

}
