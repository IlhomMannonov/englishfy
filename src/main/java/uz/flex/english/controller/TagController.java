package uz.flex.english.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.TagDto;
import uz.flex.english.servise.TagService;

import java.util.UUID;

@RestController
@RequestMapping("/tag")
public class TagController {
    @Autowired
    TagService tagService;

    @PostMapping("/saveOrEdit")
    public HttpEntity<?> saveOrEdit(TagDto tagDto){
        ApiResponse response = tagService.saveOrEdit(tagDto);
        return ResponseEntity.status(response.isSuccess()?201:409).body(response);
    }

    @GetMapping("/all")
    public HttpEntity<?> getAllOrById(@RequestParam(required = false) UUID id){
        ApiResponse response = tagService.getAllOrBYId(id);
        return ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }

    @DeleteMapping("/byId{id}")
    public HttpEntity<?> delete(@PathVariable UUID id){
        ApiResponse response = tagService.delete(id);
        return ResponseEntity.status(response.isSuccess()?202:409).body(response);
    }
}
