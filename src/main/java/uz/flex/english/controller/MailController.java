package uz.flex.english.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.servise.MailService;
import uz.flex.english.utills.AppConstants;

import java.net.URI;
import java.util.UUID;

@RestController
@RequestMapping("/api/mail")
public class MailController {

    @Autowired
    MailService mailService;

    @GetMapping("/activate")
    @ApiIgnore
    public HttpEntity<?> activate(@RequestParam String token,
                                  @RequestParam Boolean active) {
        ApiResponse response = mailService.activate(token, active);
        if(response.isSuccess()){
            return ResponseEntity.status(HttpStatus.FOUND).location(URI.create(AppConstants.FRONT_PORT)).build();
        }
        return  ResponseEntity.status(409).body(response);
    }
}
