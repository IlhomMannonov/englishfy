package uz.flex.english.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.flex.english.entity.Post;
import uz.flex.english.entity.User;
import uz.flex.english.exceptions.PageSizeException;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.PasswordDto;
import uz.flex.english.payload.UserDto;
import uz.flex.english.secret.CurrentUser;
import uz.flex.english.servise.UserService;
import uz.flex.english.utills.AppConstants;

import java.util.UUID;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UserService userService;

    @ApiOperation("user user admin yoliga yoki admin user yoliga otib ketmaslik uchun tekshiruvchi yo'l , faqat token orqali murojat")
    @GetMapping("/me")
    public HttpEntity<?> me(@CurrentUser User user) {
        return ResponseEntity.status(user != null ? 200 : 409).body(new ApiResponse(user != null, "Ok"));
    }

    //faqat user uchun
    @PatchMapping("/edit")
    public HttpEntity<?> edit(@CurrentUser User user, @RequestBody UserDto userDto) {
        ApiResponse apiResponse = userService.edit(user, userDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    // faqat admin va supper admin uchun class dto type
    @ApiOperation(value = "supper admin va admin get qila oladi")
    @GetMapping("/getAllUser")
    public HttpEntity<?> getAllUser(
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER, required = false) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE, required = false) int size,
            @RequestParam("all") boolean all
    ) throws PageSizeException {
        ApiResponse apiResponse = userService.getALl(page, size, all);
        return ResponseEntity.ok(apiResponse);
    }


    //faqat supper admin
    @ApiOperation(value = "faqat supper admin block yoki un-block qila oladi")
    @PostMapping("/block")
    public HttpEntity<?> block(
            @RequestParam(value = "userId") UUID id,
            @RequestParam(value = "enable") boolean enable
    ) {
        ApiResponse apiResponse = userService.block(id, enable);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    //faqat supper admin uchun
    @ApiOperation(value = "faqat supper admin delete qila oladi")
    @DeleteMapping("/deleteUser/{id}")
    public HttpEntity<?> deleteUser(@PathVariable UUID id) {
        ApiResponse apiResponse = userService.deleteUser(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 203 : 409).body(apiResponse);
    }

    @PostMapping("/passwordEdit")
    public HttpEntity<?> passwordEdit(@RequestBody PasswordDto dto, @CurrentUser User user) {
        ApiResponse apiResponse = userService.passwordEdit(dto, user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @GetMapping("/getMe")
    public HttpEntity<?> getMe(@CurrentUser User user) {
        ApiResponse apiResponse = userService.getMe(user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @ApiOperation(value = "faqat  admin uchun")
    @GetMapping("/get-user-by-id")
    public HttpEntity<?> getUserById(@RequestParam(value = "id") UUID id) {
        ApiResponse apiResponse = userService.getUserById(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @ApiOperation(value = "user boshqa userni get qilishi uchun")
    @GetMapping("/get-user-by-id-for-user")
    public HttpEntity<?> getUserById(@RequestParam(value = "id") UUID id, @CurrentUser User user) {
        ApiResponse apiResponse = userService.getUserByIdForUser(id, user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }
    @ApiOperation(value = "userga eng qarindosh userlarni chiqarish top 40")
    @GetMapping("/get-recommend-user")
    public HttpEntity<?> getRecommendUser(@CurrentUser User user) {
        ApiResponse apiResponse = userService.getRecommendUser(user.getId());
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }
    @ApiOperation(value = "uselarni search qilish")
    @GetMapping("/search-user-by-username")
    public HttpEntity<?> searchUserByUsername(@RequestParam(value = "username") String username) {
        ApiResponse apiResponse = userService.searchUserByUsername(username);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }
}
