package uz.flex.english.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.flex.english.entity.User;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.SolvedWordPartDto;
import uz.flex.english.secret.CurrentUser;
import uz.flex.english.servise.SolveWordService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/solveWord")
public class SolveWordController {

    @Autowired
    SolveWordService solveWordService;

    @PostMapping("/save")
    public HttpEntity<?> saveWordHistory(@RequestBody List<SolvedWordPartDto> wordPartDtoList,@CurrentUser User user) {
        ApiResponse response = solveWordService.save(wordPartDtoList, user);
        return ResponseEntity.status(response.isSuccess() ? 201 : 409).body(response);
    }

    @GetMapping("/getAll")
    public HttpEntity<?> getAllOrById(@RequestParam(required = false) UUID id) {
        ApiResponse response = solveWordService.getAllOrById(id);
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }

    @GetMapping("/getByUnitIdAndUserId")
    public HttpEntity<?> getByUnitIdAndUserId(@RequestParam UUID unitId, @CurrentUser User user) {
        ApiResponse response = solveWordService.getByUserAndUnit(unitId, user.getId());
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }

    @GetMapping("/findByUnitIdAndUserId")
    public HttpEntity<?> findByUnitIdAndUserId(@RequestParam UUID unitId, @RequestParam UUID userId) {
        ApiResponse response = solveWordService.getByUserAndUnit(unitId, userId);
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }

    @GetMapping("/getByUser")
    public HttpEntity<?> getByUser(@CurrentUser User user) {
        ApiResponse response = solveWordService.getByUser(user.getId());
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }

    @GetMapping("/findByUserId{id}")
    public HttpEntity<?> findByUserId(@PathVariable UUID id) {
        ApiResponse response = solveWordService.getByUser(id);
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }


    @PostMapping("/check")
    public HttpEntity<?> checkWord(@RequestBody SolvedWordPartDto wordPartDto) {
        ApiResponse response = solveWordService.checkWord(wordPartDto);
        return ResponseEntity.status(response.isSuccess() ? 201 : 409).body(response);
    }

}















