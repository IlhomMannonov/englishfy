package uz.flex.english.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.flex.english.entity.User;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.secret.CurrentUser;
import uz.flex.english.servise.TensesHistoryService;

import java.util.UUID;

@RestController
@RequestMapping("/api/tensesHistory")
public class TenseHistoryController {

    @Autowired
    TensesHistoryService tensesHistoryService;

    @ApiOperation(value = "user o'zining ishlaga teneses larini korish")
    @GetMapping("/myExamAllResult")
    HttpEntity<?> myExamResult(@CurrentUser User user) {
        ApiResponse apiResponse = tensesHistoryService.MyExamResult(user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @ApiOperation(value = "admin uchun userni hamma ishlagan tenses larini korish")
    @GetMapping("/getUserExam/{id}")
    HttpEntity<?> userExam(@PathVariable UUID id) {
        ApiResponse apiResponse = tensesHistoryService.userExam(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @ApiOperation(value = "user o'zi ishlagan tenses savollariga javobini korish")
    @GetMapping("/getMyTensesAnswer/{id}")
    HttpEntity<?> getMyTensesAnswer(@PathVariable UUID id, @CurrentUser User user) {
        ApiResponse apiResponse = tensesHistoryService.getMyTensesAnswer(user, id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @ApiOperation(value = "Admin userni  ishlagan tenses savollariga javobini korish")
    @GetMapping("/getUserTensesAnswer")
    HttpEntity<?> getUserTensesAnswer(
            @RequestParam("tenseId") UUID tenseId,
            @RequestParam("userId") UUID userId
    ) {
        ApiResponse apiResponse = tensesHistoryService.getUserTensesAnswer(tenseId,userId);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }
}
