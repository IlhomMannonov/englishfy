package uz.flex.english.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.LoginDto;
import uz.flex.english.payload.ResToken;
import uz.flex.english.payload.SingUpDto;
import uz.flex.english.repo.UserRepo;
import uz.flex.english.servise.AuthServise;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    UserRepo userRepo;


    @Autowired
    AuthServise authServise;


    @PostMapping("/login")      
    @ApiOperation("Login")
    public HttpEntity<?> login(@Valid @RequestBody LoginDto loginDto) {
        ResToken resToken = authServise.login(loginDto);
        return ResponseEntity.ok(resToken);
    }

    @PostMapping("/register")
    @ApiOperation(value = "Registration")
    public HttpEntity<?> login(@Valid @RequestBody SingUpDto dto){
        ApiResponse response = authServise.register(dto);
        return ResponseEntity.status(response.isSuccess()?201:409).body(response);
    }


    @PostMapping("/forgotPassword")
    public HttpEntity<?>forgot(@RequestBody LoginDto loginDto){
        ApiResponse apiResponse = authServise.forgot(loginDto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }

}
