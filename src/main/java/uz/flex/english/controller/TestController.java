package uz.flex.english.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.flex.english.entity.User;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.SolvedTestDto;
import uz.flex.english.payload.TestDto;
import uz.flex.english.secret.CurrentUser;
import uz.flex.english.servise.TestService;

import java.util.UUID;

@RestController
@RequestMapping("/api/test")
public class TestController {

    @Autowired
    TestService testService;

    @PostMapping("/saveOrEdit")
    public HttpEntity<?> saveOrEdit(@RequestBody TestDto testDto) {
        ApiResponse apiResponse = testService.saveOrEdit(testDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 201 : 409).body(apiResponse);
    }

    @ApiOperation(value = "test toplamini olish")
    @GetMapping("/get/{id}")
    public HttpEntity<?> get(@PathVariable UUID id) {
        ApiResponse apiResponse = testService.get(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }


    @GetMapping("/getAll/{levelId}")
    public HttpEntity<?> getAll(@PathVariable UUID levelId, @CurrentUser User user) {
        ApiResponse apiResponse = testService.getAll(levelId, user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @ApiOperation(value = "test toplamini o'chirish")
    @DeleteMapping("/delete/Test/{id}")
    public HttpEntity<?> delete(@PathVariable UUID id) {
        ApiResponse apiResponse = testService.deleteTest(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 203 : 409).body(apiResponse);
    }

    @DeleteMapping("/delete/question/{id}")
    public HttpEntity<?> deleteQuestion(@PathVariable UUID id) {
        ApiResponse apiResponse = testService.deleteQuestion(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 203 : 409).body(apiResponse);
    }

    @ApiOperation(value = "test questionning answerini o'chirish")
    @DeleteMapping("/delete/answer/{id}")
    public HttpEntity<?> deleteAnswer(@PathVariable UUID id) {
        ApiResponse apiResponse = testService.deleteAnswer(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 203 : 409).body(apiResponse);
    }

    @PostMapping("/exam")
    public HttpEntity<?> exam(@RequestBody SolvedTestDto solvedTestDto, @CurrentUser User user) {
        ApiResponse apiResponse = testService.exam(solvedTestDto, user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 203 : 409).body(apiResponse);
    }

}
