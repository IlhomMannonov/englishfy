package uz.flex.english.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.flex.english.exceptions.PageSizeException;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.GroupDto;
import uz.flex.english.servise.GroupService;
import uz.flex.english.utills.AppConstants;

import java.util.UUID;

@RestController

@RequestMapping("/api/group")
public class GroupController {

    @Autowired
    private GroupService groupService;

    @PostMapping("/saveOrEdit")
    public HttpEntity<?>add(@RequestBody GroupDto groupDto){
        ApiResponse apiResponse = groupService.add(groupDto);
        return ResponseEntity.status(apiResponse.isSuccess()?201:409).body(apiResponse);
    }
    @GetMapping("/getAll")
    public HttpEntity<?>getAll(
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) throws PageSizeException {
      ApiResponse apiResponse = groupService.getAll(page, size);
      return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }


    @DeleteMapping("/delete/{id}")
    public HttpEntity<?>delete(@PathVariable("id") UUID id){
        ApiResponse apiResponse = groupService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess()?203:409).body(apiResponse);
    }
    @GetMapping("/getByLevelId/{levelId}")
    public HttpEntity<?>getByLevelId(
            @PathVariable("levelId") UUID levelId,
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) throws PageSizeException {
        ApiResponse apiResponse = groupService.getByLevelId(levelId, page, size);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }
}
