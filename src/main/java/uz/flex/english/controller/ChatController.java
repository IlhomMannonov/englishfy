package uz.flex.english.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.flex.english.entity.User;
import uz.flex.english.exceptions.PageSizeException;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.DirectDto;
import uz.flex.english.payload.MessageDto;
import uz.flex.english.secret.CurrentUser;
import uz.flex.english.servise.ChatService;
import uz.flex.english.utills.AppConstants;

import java.util.UUID;

@RestController
@RequestMapping("/api/chat")
public class ChatController {
    @Autowired
    private ChatService chatService;


    /**
     * @param messageDto this is message that we want to send  /  messagening direct idsi null bo'sa user id bo'lishi shart
     * @param user       this is you
     * @param userId     bu id null  bo'lishi mumkin chunki user bu user bilan birinchi marta yozishganda direct yaratish uchun
     * @return this is response
     */
    @PostMapping("/send-message-to-user")
    public HttpEntity<?> sendMessage(@RequestBody MessageDto messageDto, @CurrentUser User user, @RequestParam(required = false) UUID userId) {
        ApiResponse apiResponse = chatService.sendMessage(messageDto, user, userId);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    /**
     * @param messageDto o
     * @param user
     * @param groupId    group id not null
     * @return message
     */
    @PostMapping("/send-message-to-group")
    public HttpEntity<?> sendMessageToGroup(@RequestBody MessageDto messageDto, @CurrentUser User user, @RequestParam(required = false) UUID groupId) {
        ApiResponse apiResponse = chatService.sendMessageToGroup(messageDto, user, groupId);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }


    /**
     * @param messageDto messageId not null
     * @param user
     * @return
     */
    @PutMapping("/update-message")
    public HttpEntity<?> updateMessage(@RequestBody MessageDto messageDto, @CurrentUser User user) {
        ApiResponse apiResponse = chatService.updateMessage(messageDto, user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/get-messages")
    public HttpEntity<?> getMessagesUserToUser(
            @RequestParam UUID directId,
            @CurrentUser User user,
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_MESSAGE_SIZE) int size

    ) throws PageSizeException {
        ApiResponse apiResponse = chatService.getMessagesUserToUser(directId, user, page, size);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/get-my-directs")
    public HttpEntity<?> getMyDirects(@CurrentUser User user,
                                      @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                      @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_MESSAGE_SIZE) int size) throws PageSizeException {
        ApiResponse apiResponse = chatService.getMyDirects(user, page, size);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/get-all-messages-by-groupId")
    public HttpEntity<?> getAllMessagesByGroupId(
            @CurrentUser User user,
            @RequestParam UUID groupId,
            @RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
            @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_MESSAGE_SIZE) int size) throws PageSizeException {
        ApiResponse apiResponse = chatService.getAllMessagesByGroupId(groupId, page, size, user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

}
