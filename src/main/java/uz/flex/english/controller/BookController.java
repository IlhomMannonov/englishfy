package uz.flex.english.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.BookDto;
import uz.flex.english.servise.BookService;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/book")
public class BookController {
    @Autowired
    BookService bookService;

    @GetMapping("/all")
    public HttpEntity<?> getAllOrById(@RequestParam(required = false) UUID id) {
        ApiResponse res = bookService.getAllOrById(id);
        return ResponseEntity.status(res.isSuccess() ? 200 : 409).body(res);
    }

    @DeleteMapping("/delete/{id}")
    public HttpEntity<?> delete(@PathVariable UUID id) {
        ApiResponse response = bookService.delete(id);
        return ResponseEntity.status(response.isSuccess() ? 202 : 409).body(response);
    }

    @PostMapping("/saveOrEdit")
    public HttpEntity<?> saveOrEdit(@Valid @RequestBody BookDto bookDto){
        ApiResponse response = bookService.saveorEdit(bookDto);
        return ResponseEntity.status(response.isSuccess()?201:409).body(response);
    }
}
