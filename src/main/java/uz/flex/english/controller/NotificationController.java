package uz.flex.english.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.flex.english.entity.User;
import uz.flex.english.exceptions.PageSizeException;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.secret.CurrentUser;
import uz.flex.english.servise.NotificationService;

import java.util.UUID;

@RestController
@RequestMapping("/api/notification")
public class  NotificationController {

    @Autowired
    private NotificationService notificationService;

    @ApiOperation(value = "user get-notifications by page")
    @GetMapping("/my-notifications")
    public HttpEntity<?>getMyNotifications(
            @CurrentUser User user,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size
    ) throws PageSizeException {
        ApiResponse apiResponse = notificationService.getMyNotifications(user, page, size);
        return ResponseEntity.ok(apiResponse);
    }

    @ApiOperation(value = "user delete-notification by id")
    @DeleteMapping("/delete-notification/{id}")
    public HttpEntity<?>deleteNotification(
            @CurrentUser User user,
            @PathVariable("id") UUID id
    ) {
       ApiResponse apiResponse= notificationService.deleteNotification(user, id);
       return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @ApiOperation(value = "user delete-all-notifications")
    @PostMapping("delete-all-notifications")
    public HttpEntity<?>deleteAllNotifications(
            @CurrentUser User user
    ) {
        ApiResponse apiResponse= notificationService.deleteAllNotifications(user);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }
    @ApiOperation(value = "user get-notifications-not-viewed")
    @GetMapping("/get-notification-not-viewed-by-page")
    public HttpEntity<?>getNotificationNotViewed(
            @CurrentUser User user,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size
    ) throws PageSizeException {
        ApiResponse apiResponse = notificationService.getNotificationNotViewed(user, page, size);
        return ResponseEntity.ok(apiResponse);
    }


    @ApiOperation(value = "admin send-all-users-notification")
    @PostMapping("/send-all-users-notification")
    public HttpEntity<?>sendAllUsersNotification(
            @RequestBody String message
    ) {
        ApiResponse apiResponse= notificationService.sendAllUsersNotification(message);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

}