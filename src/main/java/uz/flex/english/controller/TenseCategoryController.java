package uz.flex.english.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.TensesCategoryDto;
import uz.flex.english.servise.TenseCategoryService;

import java.util.UUID;

@RestController
@RequestMapping("/api/tense")
public class TenseCategoryController {

    @Autowired
    TenseCategoryService tenseCategoryService;

    @ApiOperation(value = "Tensesni saqlash yoki edit qilish", notes = "add only admin and supper admin, name va levelId = ota tances qoshish | id, name va levelId = taxrirlash | id, name, levelId va tanceId = bola categoriya qoshish")
    @PostMapping("/addOrSave")
    public HttpEntity<?> addTenses(@RequestBody TensesCategoryDto tancesDto) {
        ApiResponse apiResponse = tenseCategoryService.addTenses(tancesDto);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @ApiOperation(value = "tenses categotylarni olsih", notes = "supperId orqali ota tenses ga tegishli hamma tenseslarni korish mumkin, default ota tenseslarni ko'rish mumkin")
    @GetMapping("/getTencesCategory")
    public HttpEntity<?> getTenses(
            @RequestParam(value = "supperId", required = false) UUID supperId) {
        ApiResponse apiResponse = tenseCategoryService.getTenses(supperId);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);

    }

    @DeleteMapping("/delete/{id}")
    public HttpEntity<?> deleteTenses(@PathVariable UUID id) {
        ApiResponse apiResponse = tenseCategoryService.delete(id);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @GetMapping("/getById/{id}")
    public HttpEntity<?> getById(@PathVariable UUID id) {
        ApiResponse apiResponse = tenseCategoryService.getById(id);
        return ResponseEntity.ok(apiResponse);
    }

}
