package uz.flex.english.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.flex.english.entity.User;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.LikeDto;
import uz.flex.english.secret.CurrentUser;
import uz.flex.english.servise.LikeService;

@RestController
@RequestMapping("/api/like")
public class LikeController {
    @Autowired
    private LikeService likeService;

    @ApiOperation(value = "liked or Un-Liked for post")
    @PostMapping("/post")
    public HttpEntity<?> likePost(@CurrentUser User user, @RequestBody LikeDto likeDto) {
        ApiResponse apiResponse = likeService.likeOrUnLikedPost(user,likeDto);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @ApiOperation(value = "liked or Un-Liked for comment")
    @PostMapping("/comment")
    public HttpEntity<?>likeComment(@CurrentUser User user, @RequestBody LikeDto likeDto){
        ApiResponse apiResponse = likeService.likeOrUnlikeComment(user, likeDto);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }


}
