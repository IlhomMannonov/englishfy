package uz.flex.english.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.flex.english.entity.User;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.SolveLevelPartDto;
import uz.flex.english.secret.CurrentUser;
import uz.flex.english.servise.SolvedLevelWordService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/solveLeveWords")
public class SolveVocabularyLevelController {
    @Autowired
    SolvedLevelWordService levelWordService;


    @PostMapping("/save")
    public HttpEntity<?> save(@RequestBody List<SolveLevelPartDto> levelPartDtos, @RequestParam UUID levelId, @CurrentUser User user){
        ApiResponse response = levelWordService.save(levelPartDtos,levelId,user);
        return ResponseEntity.status(response.isSuccess()?201:409).body(response);
    }

    @GetMapping("/all")
    public HttpEntity<?> getAll(){
        ApiResponse response = levelWordService.getAll();
        return  ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }

    @GetMapping("/getAllByUser{id}")
    public HttpEntity<?> getAll(@PathVariable UUID id){
        ApiResponse response = levelWordService.getByUser(id);
        return  ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }

    @GetMapping("/getByUserAndLevel")
    public HttpEntity<?> getAllByUserAndLevel(@RequestParam UUID levelID,@RequestParam UUID userId){
        ApiResponse response = levelWordService.getByUserAndLevel(levelID, userId);
        return ResponseEntity.status(response.isSuccess()?200:409).body(response);
    }

}
