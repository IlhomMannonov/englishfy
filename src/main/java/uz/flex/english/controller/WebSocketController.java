package uz.flex.english.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import uz.flex.english.entity.Message;

@RestController
public class WebSocketController {
    @Autowired
    SimpMessagingTemplate template;

    // Handle messages from /app/chat
    @MessageMapping("/chat")
    // Sends the return value of this method to /topic/messages
    @SendTo("/topic/message")
    public Message sendMessage(@Payload Message message) {
        return message;
    }


    @PostMapping("/send")
    public HttpEntity<?> sendMessages(@RequestBody Message message) {
        //
        template.convertAndSend("/topic/message", message);
        //
        return ResponseEntity.ok("Jonatildi");
    }


}
