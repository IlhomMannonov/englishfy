package uz.flex.english.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.flex.english.entity.SolvedTest;
import uz.flex.english.entity.User;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.secret.CurrentUser;
import uz.flex.english.servise.SolvedTestService;

import java.util.UUID;

@RestController
@RequestMapping("/api/solvedTest")
public class SolvedTestController {

    @Autowired
    SolvedTestService solvedTestService;

    @GetMapping("/getMySolvedTest")
    public HttpEntity<?>getMySolvedTest(@CurrentUser User user){
        ApiResponse apiResponse = solvedTestService.getMySolvedTest(user.getId());
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @ApiOperation(value ="Admin userlarning ishlagan testlarini korishi" )
    @GetMapping("/getUserSolvedTest/{id}")
    public HttpEntity<?>getMySolvedTest(@PathVariable UUID id){
        ApiResponse apiResponse = solvedTestService.getMySolvedTest(id);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }


    @ApiOperation(value = "User test id orqali o'ziga tegishli questionlarni korish")
    @GetMapping("/getMySolvedOneTest/{id}")
    public HttpEntity<?>getMySolvedOneTest(@PathVariable UUID id, @CurrentUser User user ){
        ApiResponse apiResponse = solvedTestService.getMySolvedOneTest(id, user.getId());
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    @ApiOperation(value = "Admin test id orqali unga tegishli questionlarni korish")
    @GetMapping("/getMySolvedOneTest/{id}/{userId}")
    public HttpEntity<?>getUserSolvedOneTest(@PathVariable UUID id, @PathVariable UUID userId){
        ApiResponse apiResponse = solvedTestService.getMySolvedOneTest(id, userId);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

}
