package uz.flex.english.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.LevelDto;
import uz.flex.english.servise.LevelService;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/level")
public class LevelController {
    @Autowired
    LevelService levelService;
    //bu yul hamma IsActive ture bulganlarini qaytaradi
    @GetMapping("/getLevelTrue")
    public HttpEntity<?>getLeve(){
        ApiResponse apiResponse = levelService.getAllLevelTrue();
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }
    @GetMapping("/getOne/{id}")
    public HttpEntity<?>GetById(@PathVariable UUID id){
        ApiResponse apiResponse= levelService.getOne(id);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }
    //Add or Edit
    @PostMapping("/addLeve")
    public HttpEntity<?> addLeve(@Valid @RequestBody LevelDto levelDto){
        ApiResponse apiResponse=levelService.addLevel(levelDto);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }
    //deleteById;
    @DeleteMapping("/delete/{id}")
    public HttpEntity<?>deleteLevel(@PathVariable UUID id){
        ApiResponse apiResponse=levelService.delete(id);
        return  ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }
    // bu yul hamma levelni qaytaradi
    @GetMapping("/getAllLevel")
    public HttpEntity<?>getAllLevel(){
        ApiResponse apiResponse=levelService.getAllLeveTrueOrFalse();
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }


}
