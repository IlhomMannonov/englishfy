package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;
import uz.flex.english.entity.template.AbsUUID;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class TestQuestion extends AbsUUID {


    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Test test;
    @Type(type = "text")
    @Column(nullable = false)
    private String question;

    private Boolean active = true;

    @OneToMany(mappedBy = "testQuestion",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    List<TestAnswer>testAnswerList;

    public void setTestAnswerList(List<TestAnswer> testAnswerList) {
        for (TestAnswer testAnswer : testAnswerList) {
            testAnswer.setTestQuestion(this);
        }
        this.testAnswerList = testAnswerList;
    }
}
