package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import uz.flex.english.entity.template.AbsUUID;

import javax.persistence.*;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Message extends AbsUUID {

    @ManyToOne(optional = false)
    private User user;

    @ManyToOne
    private Message message;

    @Type(type = "text")
    private String text;

    @ManyToOne(optional = false)
    private Direct direct;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<Attachment> attachmentList;


    private boolean edited;
}
