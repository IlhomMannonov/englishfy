package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import uz.flex.english.entity.template.AbsUUID;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Spam extends AbsUUID {


    @ManyToOne(optional = true)
    private User user;

    @ManyToOne(optional = false)
    private Post post;

    @Type(type = "text")
    @Column(nullable = false)  // spam uchun text mavjud shu orqali qanday kontentda ekanligini bilish mavjud
    private String text;

}
