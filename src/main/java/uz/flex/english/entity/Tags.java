package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import uz.flex.english.entity.template.AbsEntity;

import javax.persistence.Entity;

@Data
@RequiredArgsConstructor
@Entity
public class Tags extends AbsEntity {
    private boolean active;
}
