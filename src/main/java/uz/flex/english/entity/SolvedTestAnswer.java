package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import uz.flex.english.entity.template.AbsUUID;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class SolvedTestAnswer extends AbsUUID {

    @ManyToOne(optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private SolvedTest solvedTest;

    @ManyToOne(optional = false)
    private TestQuestion testQuestion;

    @ManyToOne(optional = false)
    private TestAnswer testAnswer;
}
