package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.flex.english.entity.template.AbsUUID;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class SolvedWordHistory extends AbsUUID {

    @ManyToOne(optional = false)
    private User user;

    @ManyToOne(optional = false)
    CoinValue coinValue;

    @ManyToOne(optional = false)
    Unit unit;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "wordHistory",cascade = CascadeType.PERSIST)
    List<SolvedWordPart> wordParts;

}
