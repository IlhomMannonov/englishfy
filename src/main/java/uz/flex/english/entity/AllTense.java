package uz.flex.english.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import uz.flex.english.entity.template.AbsUUID;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class AllTense extends AbsUUID {

    @JsonIgnore
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Tense tense;

    @OneToMany(mappedBy = "allTenses", cascade = CascadeType.ALL)
    private List<AllTensePart> allTensesPartList;

    public void setAllTensesPartList(List<AllTensePart> allTensesPartList) {
        for (AllTensePart allTensesPart : allTensesPartList) {
            allTensesPart.setAllTenses(this);
        }
        this.allTensesPartList = allTensesPartList;
    }

}
