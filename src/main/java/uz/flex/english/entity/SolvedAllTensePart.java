package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.minidev.json.annotate.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import uz.flex.english.entity.template.AbsUUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class SolvedAllTensePart extends AbsUUID {
    @Column(columnDefinition="TEXT")
    private String inputWord;

    private boolean correct;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private AllTensePart allTensesPart;

    @ManyToOne(optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private SolvedAllTense solvedAllTense;

}
