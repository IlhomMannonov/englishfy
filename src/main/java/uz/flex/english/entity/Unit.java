package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.flex.english.entity.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Unit extends AbsEntity {

    @ManyToOne(optional = false)
    private Book book;

    @Column(nullable = false)
    private Integer unitNumber;

    private boolean active;
}
