package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.flex.english.entity.template.AbsUUID;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class SolvedTest extends AbsUUID {

    @ManyToOne(optional = false)
    private Test test;

    @ManyToOne(optional = false)
    private User user;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "solvedTest")
    private List<SolvedTestAnswer> solvedTestAnswerList;

    public void setSolvedTestAnswerList(List<SolvedTestAnswer> solvedTestAnswerList) {
        for (SolvedTestAnswer solvedTestAnswer : solvedTestAnswerList) {
            solvedTestAnswer.setSolvedTest(this);
        }
        this.solvedTestAnswerList = solvedTestAnswerList;
    }
}
