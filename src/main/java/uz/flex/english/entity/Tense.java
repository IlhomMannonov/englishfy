package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.flex.english.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Tense extends AbsEntity {

    @ManyToOne(optional = false)
    private TenseCategory tenseCategory;

    @Column(nullable = false)
    private Integer amount;

    private boolean active;

    @OneToMany(mappedBy = "tense",cascade = CascadeType.ALL)
    private List<AllTense> allTenses;


    public void setAllTenses(List<AllTense> allTenses) {
        for (AllTense allTens : allTenses) {
            allTens.setTense(this);
        }
        this.allTenses = allTenses;
    }
}
