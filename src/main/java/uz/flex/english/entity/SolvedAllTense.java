package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.minidev.json.annotate.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import uz.flex.english.entity.template.AbsUUID;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class SolvedAllTense extends AbsUUID {

    @JsonIgnore
    @ManyToOne(optional = false,fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private SolvedTense solvedTense;
    private boolean correct;

    @OneToMany(mappedBy = "solvedAllTense", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<SolvedAllTensePart> solvedAllTensePartList;

    public void setSolvedAllTensePartList(List<SolvedAllTensePart> solvedAllTensePartList) {
        for (SolvedAllTensePart solvedAllTensePart : solvedAllTensePartList) {
            solvedAllTensePart.setSolvedAllTense(this);
        }
        this.solvedAllTensePartList = solvedAllTensePartList;
    }
}
