package uz.flex.english.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import uz.flex.english.entity.template.AbsUUID;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class AllTensePart extends AbsUUID {
    @Column(columnDefinition="TEXT")
    private String text;
    @Column(columnDefinition="TEXT")
    private String question;
    private Boolean key;

    @JsonIgnore
    @ManyToOne(optional = false,fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private AllTense allTenses;

}
