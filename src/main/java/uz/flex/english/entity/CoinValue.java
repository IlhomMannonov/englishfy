package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.flex.english.entity.enums.CoinType;
import uz.flex.english.entity.template.AbsLong;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class CoinValue extends AbsLong {
    private long amount;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, unique = true)
    private CoinType coinType;



}
