package uz.flex.english.entity;

import lombok.*;
import org.hibernate.annotations.Type;
import uz.flex.english.entity.template.AbsUUID;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Post extends AbsUUID {

    @Type(type = "text")
    private String description;

    @OneToMany
    private List<Attachment> attachmentList;

    @ManyToOne(optional = false)
    private User user;

    @ManyToMany
    private List<Tags> tags;

    private boolean active = true;

}
