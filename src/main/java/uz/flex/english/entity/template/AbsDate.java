package uz.flex.english.entity.template;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@MappedSuperclass
public abstract class AbsDate extends AbsAudity implements Serializable {

    @CreationTimestamp
    @Column(updatable = false)
    private Timestamp createdat;

    @UpdateTimestamp
    private Timestamp updatedat;
}
