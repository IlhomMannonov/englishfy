package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.flex.english.entity.template.AbsUUID;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Notification extends AbsUUID {
    private String message;

    @ManyToOne(optional = false)
    private User user;

    boolean viewed;

    public Notification(String message, User user) {
        this.message = message;
        this.user = user;
    }
}
