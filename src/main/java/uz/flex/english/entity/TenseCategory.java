package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.flex.english.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Collection;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class TenseCategory extends AbsEntity {

    @ManyToOne
    private Level level;

    // agar tense null bolsa va level bolsa bu ota tense categoriya
    @ManyToOne
    private TenseCategory tenseCategory;

}
