package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.flex.english.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Test extends AbsEntity {

    @ManyToOne(optional = false)
    private Level level;

    private Long coinAmount;

    @OneToMany(mappedBy = "test", cascade = CascadeType.ALL)
    List<TestQuestion> testQuestionList;

    public void setTestQuestionList(List<TestQuestion> testQuestionList) {
        for (TestQuestion testQuestion : testQuestionList) {
            testQuestion.setTest(this);
        }
        this.testQuestionList = testQuestionList;
    }
}
