package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import uz.flex.english.entity.template.AbsEntity;
import uz.flex.english.entity.template.AbsUUID;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class SolveLevelWordHistory extends AbsEntity {

    @ManyToOne(optional = false)
    private User user;


    @ManyToOne(optional = false)
    Level level;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "levelWordHistory",cascade = CascadeType.PERSIST)
    List<SolvedLevelPart> levelParts;

}
