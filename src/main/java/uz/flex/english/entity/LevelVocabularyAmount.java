package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.flex.english.entity.template.AbsUUID;

import javax.persistence.Entity;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class LevelVocabularyAmount extends AbsUUID {

   private  int levelAmount;
   private  int wordAmount;
   private  boolean active;
}
