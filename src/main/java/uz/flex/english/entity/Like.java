package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.flex.english.entity.template.AbsUUID;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "likes")
public class Like extends AbsUUID {

    @ManyToOne
    private Post post;

    // like commentga  yoki postga tegishli bo'lishi mumkin

    @ManyToOne
    private Comment comment;

    @ManyToOne(optional = false)
    private User user;


}
