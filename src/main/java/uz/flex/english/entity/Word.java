package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.flex.english.entity.template.AbsUUID;

import javax.persistence.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(name = "UniqueWordEngAndWordUz",columnNames = {"wordEng","wordUz"})})
public class Word extends AbsUUID {

    @Column(nullable = false)
    private String wordUz;

    @Column(nullable = false)
    private String wordEng;
    private Boolean active;
    @ManyToOne(optional = false)
    private Unit unit;

    public Word(String wordUz, String wordEng, Boolean active) {
        this.wordUz = wordUz;
        this.wordEng = wordEng;
        this.active = active;
    }

}
