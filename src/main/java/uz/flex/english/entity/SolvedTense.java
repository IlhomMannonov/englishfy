package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.flex.english.entity.template.AbsUUID;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class SolvedTense extends AbsUUID {
    @ManyToOne(optional = false)
    private User user;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Tense tense;

    @OneToMany(mappedBy = "solvedTense", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<SolvedAllTense> solvedAllTenseList;

    public void setSolvedAllTenseList(List<SolvedAllTense> solvedAllTenseList) {
        for (SolvedAllTense solvedAllTense : solvedAllTenseList) {
            solvedAllTense.setSolvedTense(this);
        }
        this.solvedAllTenseList = solvedAllTenseList;
    }
}
