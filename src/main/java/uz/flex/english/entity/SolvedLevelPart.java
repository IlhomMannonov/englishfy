package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import uz.flex.english.entity.template.AbsUUID;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class SolvedLevelPart extends AbsUUID {
    private String inputWord;
    @ManyToOne(optional = false)
    private Word word;
    @ManyToOne(optional = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private SolveLevelWordHistory levelWordHistory;
    private boolean correct;
}
