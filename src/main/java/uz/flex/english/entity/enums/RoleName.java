package uz.flex.english.entity.enums;

public enum RoleName {
    ROLE_SUPPER_ADMIN,
    ROLE_ADMIN,
    ROLE_CUSTOMER
}
