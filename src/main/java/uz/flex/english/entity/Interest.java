package uz.flex.english.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.flex.english.entity.template.AbsUUID;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(name = "UniqueUserIdAndTagsId",columnNames = {"user_id","tags_id"})})
public class Interest extends AbsUUID {

    @ManyToOne(optional = false)
    private User user;


    @ManyToOne(optional = false)
    private Tags tags;

    @ManyToOne(optional = false)
    private Post post;

}
