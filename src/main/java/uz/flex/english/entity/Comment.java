package uz.flex.english.entity;

import lombok.*;
import org.hibernate.annotations.Type;
import uz.flex.english.entity.template.AbsUUID;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Comment extends AbsUUID {

    @ManyToOne(optional = false)
    private User user;
    
    @ManyToOne(optional = false)
    private Post post;

    @Type(type = "text")
    private String text;


}
