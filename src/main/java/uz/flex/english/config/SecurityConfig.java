package uz.flex.english.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import uz.flex.english.secret.JwtFilter;
import uz.flex.english.servise.LoadService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        prePostEnabled = true
)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    LoadService loadService;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(loadService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public JwtFilter jwtFilter() {
        return new JwtFilter();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .cors().and().csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(
                        "/api/auth/**",

                        "/swagger-ui/**",
                        "/api/mail/**",
                        "/v2/api-docs",
                        "/swagger-resources/**",
                        "/configuration/ui",
                        "/configuration/security",
                        "/swagger-ui.html",
                        "/webjars/**",
                        "/api/user/search-user-by-username/*"
                )
                .permitAll()
                .antMatchers(

                        "/api/tensesHistory/getUserExam/*",
                        "/api/tensesHistory/getUserTensesAnswer",
                        "/api/tenses/saveOrEdit",
                        "/api/tenses/deleteTense/*",
                        "/api/tenses/deleteAllTense/*",
                        "/api/tenses/deleteAllTensePart/*",
                        "/api/tenses/getTenseByTenseCategoryForAdmin/*",
                        "/api/test/saveOrEdit",
                        "/api/test/get/*",
                        "/api/test/delete/Test/*",
                        "/api/test/delete/question/*",
                        "/api/test/delete/answer/*",
                        "/api/user/getAllUser"
                ).hasAnyRole("ADMIN", "SUPPER_ADMIN")
                .antMatchers(
                        "/api/user/get-user-by-id/*",
                        "/api/user/block/*"
                ).hasAnyRole("SUPPER_ADMIN")
                .anyRequest()
                .authenticated();


        http.addFilterBefore(jwtFilter(), UsernamePasswordAuthenticationFilter.class);
    }


}
