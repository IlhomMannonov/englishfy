package uz.flex.english.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.flex.english.entity.CoinValue;
import uz.flex.english.entity.ContentType;
import uz.flex.english.entity.Role;
import uz.flex.english.entity.User;
import uz.flex.english.entity.enums.CoinType;
import uz.flex.english.entity.enums.RoleName;
import uz.flex.english.repo.CoinValueRepo;
import uz.flex.english.repo.ContentTypeRepo;
import uz.flex.english.repo.RoleRepo;
import uz.flex.english.repo.UserRepo;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@Component
public class DataLoader implements CommandLineRunner {
    @Autowired
    UserRepo userRepo;

    @Autowired
    RoleRepo roleRepo;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    CoinValueRepo coinValueRepo;
    @Autowired
    ContentTypeRepo contentTypeRepo;

    @Value(value = "${spring.sql.init.mode}")
    private String run;

    @Override
    public void run(String... args) throws Exception {
        if (run.equals("always")) {
            coinValueRepo.save(new CoinValue(2, CoinType.WORD));
            coinValueRepo.save(new CoinValue(5, CoinType.USERNAME));


            final Role roleAdmin = roleRepo.save(new Role(null, RoleName.ROLE_ADMIN));
            final Role roleSupperAdmin = roleRepo.save(new Role(null, RoleName.ROLE_SUPPER_ADMIN));
            final Role roleCustomer = roleRepo.save(new Role(null, RoleName.ROLE_CUSTOMER));
            User user = new User("supperadmin",
                    "supperadmin@gmail.com",
                    passwordEncoder.encode("123456"),
                    new HashSet<>(List.of(roleSupperAdmin)));

                    new HashSet<>(Collections.singleton(roleSupperAdmin));
            user.setEnabled(true);
            userRepo.save(user);
            User user1 = new User("admin",
                    "admin@gmail.com",
                    passwordEncoder.encode("123456"),
                    new HashSet<>(List.of(roleAdmin)));
            user.setEnabled(true);
            userRepo.save(user1);
            User user2 = new User("customer",
                    "customer@gmail.com",
                    passwordEncoder.encode("123456"),
                    new HashSet<>(List.of(roleCustomer)));
            user.setEnabled(true);
            userRepo.save(user2);

         contentTypeRepo.save(new ContentType("image/png"));
         contentTypeRepo.save(new ContentType("image/apng"));
         contentTypeRepo.save(new ContentType("image/avif"));
         contentTypeRepo.save(new ContentType("image/jpg"));
         contentTypeRepo.save(new ContentType("image/jpeg"));
         contentTypeRepo.save(new ContentType("image/webp"));
         contentTypeRepo.save(new ContentType("video/mp4"));
         contentTypeRepo.save(new ContentType("video/msvideo"));
         contentTypeRepo.save(new ContentType("video/x-msvideo"));
         contentTypeRepo.save(new ContentType("video/avs-video"));
        }
    }
}
