package uz.flex.english;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = {"http://localhost:3001"})
@SpringBootApplication
public class EnlishBackendApplication {


    public static void main(String[] args)  {
        SpringApplication.run(EnlishBackendApplication.class, args);
    }

}
