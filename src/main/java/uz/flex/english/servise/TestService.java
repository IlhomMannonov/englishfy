package uz.flex.english.servise;

import org.springframework.stereotype.Service;
import uz.flex.english.entity.*;
import uz.flex.english.payload.*;
import uz.flex.english.payload.interfaseDto.TestInterfaceDto;
import uz.flex.english.repo.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TestService {

    private final LevelRepo levelRepo;
    private final TestRepo testRepo;
    private final TestQuestionRepo testQuestionRepo;
    private final TestAnswerRepo testAnswerRepo;
    private final SolvedTestRepo solvedTestRepo;
    private final SolvedTestAnswerRepo solvedTestAnswerRepo;
    private final UserRepo userRepo;


    public TestService(LevelRepo levelRepo, TestRepo testRepo, TestQuestionRepo testQuestionRepo, TestAnswerRepo testAnswerRepo, SolvedTestRepo solvedTestRepo, SolvedTestAnswerRepo solvedTestAnswerRepo, UserRepo userRepo) {
        this.levelRepo = levelRepo;
        this.testRepo = testRepo;
        this.testQuestionRepo = testQuestionRepo;
        this.testAnswerRepo = testAnswerRepo;
        this.solvedTestRepo = solvedTestRepo;
        this.solvedTestAnswerRepo = solvedTestAnswerRepo;
        this.userRepo = userRepo;
    }

    public ApiResponse saveOrEdit(TestDto dto) {
        Optional<Level> optionalLevel = levelRepo.findById(dto.getLevelId());
        if (optionalLevel.isEmpty())
            return new ApiResponse(false, "Not found");

        Test test = testFromDto(dto);
        test.setLevel(optionalLevel.get());

        try {
            testRepo.save(test);
            return new ApiResponse(true, dto.getId() != null ? "Edited" : "Saved");
        } catch (Exception e) {
            return new ApiResponse(false, dto.getId() != null ? "Not Edited" : "Not Saved");

        }
    }

    public ApiResponse get(UUID id) {
        Optional<Test> optionalTest = testRepo.findById(id);
        if (optionalTest.isEmpty())
            return new ApiResponse(false, "Test not found");

        TestDto testDto = dtoFromTest(optionalTest.get());
        return new ApiResponse(true, "ok", testDto);
    }

    public ApiResponse deleteTest(UUID id) {

        try {
            testRepo.deleteById(id);
            return new ApiResponse(true, "deleted");
        } catch (Exception e) {
            return new ApiResponse(false, "Not deleted this test was solved by users ");

        }
    }

    public ApiResponse deleteQuestion(UUID id) {
        try {
            testQuestionRepo.deleteById(id);
            return new ApiResponse(true, "deleted");
        } catch (Exception e) {
            return new ApiResponse(false, "Not deleted this test was solved by users ");

        }

    }

    public ApiResponse deleteAnswer(UUID id) {
        try {
            testAnswerRepo.deleteById(id);
            return new ApiResponse(true, "deleted");
        } catch (Exception e) {
            return new ApiResponse(false, "Not deleted this test was solved by users ");

        }
    }

    public ApiResponse exam(SolvedTestDto dto, User user) {

        SolvedTest solvedTest = solvedTestFromDto(dto);
        solvedTest.setUser(user);
        try {
            SolvedTest save = solvedTestRepo.save(solvedTest);
            Integer testAmount = testQuestionRepo.getTestQuestionCountByTestId(save.getTest().getId());
            Integer successAmount = solvedTestAnswerRepo.countBySolvedTest_AndTestAnswerCorrect(save, true);

            if (solvedTestRepo.existsByUserAndTest(user.getId(), save.getTest().getId())) {
                user.setCoin(user.getCoin() + save.getTest().getCoinAmount()*successAmount);
                userRepo.save(user);
            }
            ExamResponse examResponse = new ExamResponse(
                    testAmount,
                    successAmount, (double) ((successAmount * 100) / testAmount)
            );
            return new ApiResponse(true, "success", examResponse);
        } catch (Exception e) {

            return new ApiResponse(false, "bad");
        }

    }

    public ApiResponse getAll(UUID levelId, User user) {
        List<TestInterfaceDto> all = testRepo.getAllByUserIdAndLevelId(user.getId(), levelId);
        return new ApiResponse(true,"ok",all);
    }

    //DTO

    public TestAnswerDto dtoFromTestAnswer(TestAnswer testAnswer) {
        TestAnswerDto dto = new TestAnswerDto();
        dto.setText(testAnswer.getText());
        dto.setCorrect(testAnswer.isCorrect());
        dto.setId(testAnswer.getId());
        return dto;
    }

    public TestQuestionDto dtoFromTestQuestion(TestQuestion testQuestion) {
        TestQuestionDto dto = new TestQuestionDto();
        dto.setQuestion(testQuestion.getQuestion());
        dto.setActive(testQuestion.getActive());
        dto.setId(testQuestion.getId());
        dto.setTestAnswerDtoList(testQuestion.getTestAnswerList().stream().map(this::dtoFromTestAnswer).collect(Collectors.toList()));
        return dto;
    }

    public TestDto dtoFromTest(Test test) {
        TestDto dto = new TestDto();
        dto.setName(test.getName());
        dto.setLevelId(test.getLevel().getId());
        dto.setTestQuestionDtoList(test.getTestQuestionList().stream().map(this::dtoFromTestQuestion).collect(Collectors.toList()));
        dto.setId(test.getId());
        return dto;
    }

    public TestAnswer testAnswerFromDto(TestAnswerDto dto) {
        TestAnswer testAnswer = new TestAnswer();

        if (dto.getId() != null)
            testAnswer = testAnswerRepo.getById(dto.getId());

        testAnswer.setText(dto.getText());
        testAnswer.setCorrect(dto.isCorrect());
        testAnswer.setId(dto.getId());
        return testAnswer;
    }

    public TestQuestion testQuestionFromDto(TestQuestionDto dto) {
        TestQuestion testQuestion = new TestQuestion();

        if (dto.getId() != null)
            testQuestion = testQuestionRepo.getById(dto.getId());

        testQuestion.setQuestion(dto.getQuestion());
        testQuestion.setActive(dto.isActive());
        testQuestion.setTestAnswerList(dto.getTestAnswerDtoList().stream().map(this::testAnswerFromDto).collect(Collectors.toList()));
        testQuestion.setId(dto.getId());
        return testQuestion;
    }

    public Test testFromDto(TestDto dto) {
        Test test = new Test();
        if (test.getId() != null)
            test = testRepo.getById(dto.getId());

        test.setName(dto.getName());
        test.setTestQuestionList(dto.getTestQuestionDtoList().stream().map(this::testQuestionFromDto).collect(Collectors.toList()));
        return test;
    }

    // Solved Dto

    public SolvedTest solvedTestFromDto(SolvedTestDto dto) {
        SolvedTest solvedTest = new SolvedTest();
        solvedTest.setTest(testRepo.getById(dto.getTestDto().getId()));
        solvedTest.setSolvedTestAnswerList(dto.getSolvedTestAnswerDtoList().stream().map(this::solvedTestAnswerFromDto).collect(Collectors.toList()));
        return solvedTest;
    }

    public SolvedTestAnswer solvedTestAnswerFromDto(SolvedTestAnswerDto dto) {
        SolvedTestAnswer solvedTestAnswer = new SolvedTestAnswer();
        solvedTestAnswer.setId(dto.getId());
//        solvedTestAnswer.setSolvedTest(solvedTestRepo.getById(dto.getSolvedTestDto().getId()));
        solvedTestAnswer.setTestAnswer(testAnswerRepo.getById(dto.getTestAnswerDto().getId()));
        solvedTestAnswer.setTestQuestion(testQuestionRepo.getById(dto.getTestQuestionDto().getId()));
        return solvedTestAnswer;
    }



}
