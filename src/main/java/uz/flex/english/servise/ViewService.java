package uz.flex.english.servise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.flex.english.entity.Views;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.ViewDto;
import uz.flex.english.repo.PostRepo;
import uz.flex.english.repo.UserRepo;
import uz.flex.english.repo.ViewRepo;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;



@Service
public class ViewService {
    @Autowired
    ViewRepo viewRepo;

    @Autowired
    PostService postService;

    @Autowired
    UserService userService;

    @Autowired
    UserRepo userRepo;

    @Autowired
    PostRepo postRepo;

    public ApiResponse save(ViewDto dto) {
        try {
            if(!viewRepo.existsByUserIdAndPostId(dto.getUserDto().getId(),dto.getPostDto().getId())){
                Views view = new Views();
                view.setPost(postRepo.getById(dto.getPostDto().getId()));
                view.setUser(userRepo.getById(dto.getUserDto().getId()));
                viewRepo.save(view);
                return new ApiResponse(true, "Post has been viewed");
            }
        }
        catch (Exception e){
            return new ApiResponse(false,e.getMessage());
        }
        return new ApiResponse(true,"Viewed again");
    }

    public ApiResponse getByPost(UUID id) {
        List<ViewDto> collect = viewRepo.getAllByPostId(id).stream().map(this::generateViewDto).collect(Collectors.toList());
        return new ApiResponse(true,"Post views",collect,collect.size());
    }

    ViewDto generateViewDto(Views view){
        return new ViewDto(view.getId(),postService.generatePostDto(view.getPost()),userService.generateUserDto(view.getUser()));
    }
}
