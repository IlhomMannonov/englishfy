package uz.flex.english.servise;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.flex.english.entity.*;
import uz.flex.english.payload.*;
import uz.flex.english.payload.interfaseDto.TenseInterfaceDto;
import uz.flex.english.repo.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TenseService {
    @Autowired

    private final TenseRepo tenseRepo;
    private final AllTensePartRepo allTensePartRepo;
    private final TenseCategoryRepo tenseCategoryRepo;
    private final AllTenseRepo allTenseRepo;
    private final TenseCategoryService tenseCategoryService;
    private final SolvedTenseRepo solvedTenseRepo;
    private final UserRepo userRepo;


    public TenseService(TenseRepo tenseRepo, AllTensePartRepo allTensePartRepo, TenseCategoryRepo tenseCategoryRepo, AllTenseRepo allTenseRepo1, TenseCategoryService tenseCategoryService, SolvedTenseRepo solvedTenseRepo, SolvedAllTensePartRepo solvedAllTensePartR, UserRepo userRepo) {
        this.tenseRepo = tenseRepo;
        this.allTensePartRepo = allTensePartRepo;
        this.tenseCategoryRepo = tenseCategoryRepo;
        this.allTenseRepo = allTenseRepo1;
        this.tenseCategoryService = tenseCategoryService;
        this.solvedTenseRepo = solvedTenseRepo;
        this.userRepo = userRepo;
    }

    public ApiResponse saveOrEdit(TenseDto tenseDto) {


        Optional<TenseCategory> optionalTenseCategory = tenseCategoryRepo.findById(tenseDto.getTensesCategoryDto().getId());
        if (optionalTenseCategory.isEmpty())
            return new ApiResponse(false, "Not found TenseCategory");

        Tense tense = new Tense();

        if (tenseDto.getId() != null)
            tense = tenseRepo.getById(tenseDto.getId());


        tense = tenseFromTenseDto(tenseDto);
        tense.setTenseCategory(optionalTenseCategory.get());

        try {
            tenseRepo.save(tense);
            return new ApiResponse(true, tenseDto.getId() != null ? "Edited" : "saved");
        } catch (Exception e) {
            return new ApiResponse(false, tenseDto.getId() != null ? "Not Edited" : "Not saved");

        }

    }


    public ApiResponse get(UUID id) {
        Optional<Tense> optionalTense = tenseRepo.findById(id);
        if (optionalTense.isEmpty())
            return new ApiResponse(false, "Not found tense");

        return new ApiResponse(true, "ok", dtoFromTense(optionalTense.get()));
    }

    public ApiResponse getByCategoryId(UUID id, User user) {

        List<TenseInterfaceDto> tenses = tenseRepo.getAllByTenseCategoryIdAndUserId(id, user.getId());
        return new ApiResponse(true, "ok", tenses);

    }

    public Tense tenseFromTenseDto(TenseDto dto) {
        Tense tense = new Tense();

        if (dto.getId() != null)
            tense = tenseRepo.getById(dto.getId());
        tense.setActive(dto.isActive());
        tense.setAmount(dto.getAmount());
        tense.setName(dto.getName());
        tense.setAllTenses(dto.getAllTenseDtoList().stream().map(this::allTenseFromDto).collect(Collectors.toList()));
        return tense;
    }

    public ApiResponse delete(UUID id) {
        try {
            tenseRepo.deleteById(id);
            return new ApiResponse(true, "Deleted");
        } catch (Exception e) {
            return new ApiResponse(false, "Not deleted this tense was solved by users");
        }
    }

    @Transactional
    public ApiResponse exam(SolvedTenseDto solvedTenseDto, User user) {

        Optional<Tense> optionalTense = tenseRepo.findById(solvedTenseDto.getTenseDto().getId());
        if (optionalTense.isEmpty())
            return new ApiResponse(false, "Not found");
        Tense tense = optionalTense.get();

        List<SolvedAllTense> solvedAllTenseList = new ArrayList<>();
        for (SolvedAllTenseDto solvedAllTenseDto : solvedTenseDto.getSolvedAllTenseDtoList()) {
            SolvedAllTense solvedAllTense = new SolvedAllTense();
            List<SolvedAllTensePart> solvedAllTensePartList = new ArrayList<>();
            for (SolvedAllTensePartDto solvedAllTensePartDto : solvedAllTenseDto.getSolvedAllTensePartDtoList()) {
                SolvedAllTensePart solvedAllTensePart = new SolvedAllTensePart();

                AllTensePart allTensePart = allTensePartRepo.getById(solvedAllTensePartDto.getId());
                if (solvedAllTensePartDto.getInputWord().equals(allTensePart.getText())) {
                    solvedAllTense.setCorrect(true);
                    solvedAllTensePart.setCorrect(true);
                } else {
                    solvedAllTensePart.setCorrect(false);
                    solvedAllTense.setCorrect(false);
                }
                solvedAllTensePart.setInputWord(solvedAllTensePartDto.getInputWord());
                solvedAllTensePart.setAllTensesPart(allTensePart);
                solvedAllTensePartList.add(solvedAllTensePart);
            }
            solvedAllTense.setSolvedAllTensePartList(solvedAllTensePartList);

            if (solvedAllTense.isCorrect() && !solvedTenseRepo.existsByTenseAndUser(tense, user)) {
                user.setCoin(user.getCoin() + tense.getAmount());
                userRepo.save(user);
            }
            solvedAllTenseList.add(solvedAllTense);
        }

        SolvedTense solvedTense = new SolvedTense();
        solvedTense.setTense(tense);
        solvedTense.setSolvedAllTenseList(solvedAllTenseList);
        solvedTense.setUser(user);

        try {
            SolvedTense save = solvedTenseRepo.save(solvedTense);
            Integer solvedAmount = solvedTenseRepo.getSolvedAmount(save.getId(), user.getId());
            if (solvedAmount == 0)
                solvedTenseRepo.delete(save);
            double present = (double) (solvedAmount * 100) / save.getSolvedAllTenseList().size();
            return new ApiResponse(true, solvedAmount == 0 ? "Iltimos Qaytadan urinib ko'ring" : "ok", new ExamResponse(save.getSolvedAllTenseList().size(), solvedAmount, present));
        } catch (Exception e) {
            return new ApiResponse(false, "bad");

        }


    }

    public ApiResponse deleteAllTense(UUID id) {
        try {
            allTenseRepo.deleteById(id);
            return new ApiResponse(true, "Deleted");
        } catch (Exception e) {
            return new ApiResponse(false, "Not deleted this AllTense was solved by users");
        }
    }

    public ApiResponse deleteAllTensePart(UUID id) {
        try {
            allTensePartRepo.deleteById(id);
            return new ApiResponse(true, "Deleted");
        } catch (Exception e) {
            return new ApiResponse(false, "Not deleted this AllTensePart was solved by users");
        }
    }

    public ApiResponse getAllByTenseCategoryId(UUID id) {
        List<TenseInterfaceDto> dtoList = tenseRepo.getAllByTenseCategoryId(id);
        return new ApiResponse(true, "ok", dtoList);

    }

    // Dto
    public AllTense allTenseFromDto(AllTenseDto dto) {

        AllTense allTense = new AllTense();

        if (dto.getId() != null) {
            allTense = allTenseRepo.getById(dto.getId());
        }
        allTense.setAllTensesPartList(dto.getTensePartDtoList()
                .stream()
                .map(this::allTensesPartFromDto)
                .collect(Collectors.toList()));
        return allTense;
    }

    public AllTensePart allTensesPartFromDto(AllTensePartDto dto) {
        AllTensePart allTensePart = new AllTensePart();
        if (dto.getId() != null) {
            allTensePart = allTensePartRepo.getById(dto.getId());
        }
        allTensePart.setText(dto.getText());
        allTensePart.setQuestion(dto.getQuestion());
        allTensePart.setKey(dto.isKey());

        return allTensePart;
    }

    public TenseDto dtoFromTense(Tense tense) {
        TenseDto tenseDto = new TenseDto();
        tenseDto.setActive(tense.isActive());
        tenseDto.setId(tense.getId());
        tenseDto.setName(tense.getName());
        tenseDto.setAllTenseDtoList(tense.getAllTenses().stream().map(this::dtoFromAllTense).collect(Collectors.toList()));
        tenseDto.setTensesCategoryDto(tenseCategoryService.tensesToDto(tense.getTenseCategory()));
        return tenseDto;
    }

    public AllTenseDto dtoFromAllTense(AllTense allTense) {
        AllTenseDto allTenseDto = new AllTenseDto();
        allTenseDto.setId(allTense.getId());
        allTenseDto.setTensePartDtoList(allTense.getAllTensesPartList().stream().map(this::dtoFromAllTensePart).collect(Collectors.toList()));
        return allTenseDto;
    }

    public AllTensePartDto dtoFromAllTensePart(AllTensePart allTensePart) {
        return new AllTensePartDto(
                allTensePart.getId(),
                allTensePart.getText(),
                allTensePart.getQuestion(),
                allTensePart.getKey()
        );
    }


}
