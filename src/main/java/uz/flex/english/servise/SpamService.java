package uz.flex.english.servise;

import org.springframework.stereotype.Service;
import uz.flex.english.entity.Spam;
import uz.flex.english.entity.User;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.SpamDto;
import uz.flex.english.repo.PostRepo;
import uz.flex.english.repo.SpamRepo;

@Service
public class SpamService {

    private final SpamRepo spamRepo;
    private final PostRepo postRepo;

    public SpamService(SpamRepo spamRepo, PostRepo postRepo) {
        this.spamRepo = spamRepo;
        this.postRepo = postRepo;
    }

    public ApiResponse sendSpam(User user, SpamDto spamDto) {

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                spamRepo.save(spamFromDto(spamDto, user));
            }
        });
        thread.start();
        return new ApiResponse(true, "your spam was sent");
    }

    public Spam spamFromDto(SpamDto spamDto, User user) {
        Spam spam = new Spam();
        spam.setPost(postRepo.getById(spamDto.getPostId()));
        spam.setUser(user);
        spam.setText(spamDto.getText());
        return spam;
    }
}


