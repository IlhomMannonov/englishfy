package uz.flex.english.servise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.flex.english.entity.*;
import uz.flex.english.payload.*;
import uz.flex.english.repo.LevelRepo;
import uz.flex.english.repo.SolveLevelWordHistoryRepo;
import uz.flex.english.repo.SolvedWordHistoryRepo;
import uz.flex.english.repo.WordRepo;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class SolvedLevelWordService {
    @Autowired
    WordService wordService;
    @Autowired
    LevelService levelService;
    @Autowired
    UserService userService;
    @Autowired
    LevelRepo levelRepo;
    @Autowired
    WordRepo wordRepo;
    @Autowired
    SolveLevelWordHistoryRepo historyRepo;


    SolveLevelPartDto generateSolvedWordPartDto(SolvedLevelPart part) {
        return new SolveLevelPartDto(part.getId(),part.getInputWord(),wordService.generateWordDto(part.getWord()),part.isCorrect());
    }

    SolveLevelWordHistoryDto generateLevelWordHistoryDto(SolveLevelWordHistory history){
        List<SolveLevelPartDto> collect = history.getLevelParts().stream().map(this::generateSolvedWordPartDto).collect(Collectors.toList());
        return  new SolveLevelWordHistoryDto(history.getId(), history.getName(),userService.generateUserDto(history.getUser()), levelService.generateLevelDto(history.getLevel()),collect);
    }

    public ApiResponse save(List<SolveLevelPartDto> partDtos, UUID levelId, User user) {
        SolveLevelWordHistory history = new SolveLevelWordHistory();
        Level level = levelRepo.getById(levelId);
        List<SolvedLevelPart> levelParts = new ArrayList<>();
        int rightAnswers=0;
        for (SolveLevelPartDto partDto : partDtos) {
            Word word = wordRepo.getById(partDto.getWordDto().getId());
            boolean equals = partDto.getInputWord().equals(word.getWordEng());
            levelParts.add( new SolvedLevelPart(partDto.getInputWord(),word,history,equals));
           if(equals)
               rightAnswers++;
        }

        history.setLevel(level);
        history.setUser(user);
        history.setLevelParts(levelParts);
        historyRepo.save(history);
        ExamResponse response = new ExamResponse(partDtos.size(), rightAnswers,  (double) (rightAnswers * 100) / partDtos.size());
        return new ApiResponse(true,"Successfully saved!",response);
    }

    public ApiResponse getAll() {
        List<SolveLevelWordHistoryDto> collect = historyRepo.findAll().stream().map(this::generateLevelWordHistoryDto).collect(Collectors.toList());
        return new ApiResponse(true,"All Solved Vocabulary exercise histories!",collect,collect.size());
    }

    public ApiResponse getByUserAndLevel(UUID levelId, UUID userId) {
        //User va level boyicha olib kelish
        List<SolveLevelWordHistoryDto> collect = historyRepo.getAllByUserIdAndLevelId(levelId, userId).stream().map(this::generateLevelWordHistoryDto).collect(Collectors.toList());
        return new ApiResponse(true,"All Solved Vocabulary exercise histories of user according to level!",collect,collect.size());
    }

    public ApiResponse getByUser(UUID userId) {
        List<SolveLevelWordHistoryDto> collect = historyRepo.getAllByUserId(userId).stream().map(this::generateLevelWordHistoryDto).collect(Collectors.toList());
        return new ApiResponse(true,"All Solved Vocabulary exercise histories of user!",collect,collect.size());
    }
}
