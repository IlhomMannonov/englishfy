package uz.flex.english.servise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.flex.english.entity.Book;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.BookDto;
import uz.flex.english.repo.AttachmentRepo;
import uz.flex.english.repo.BookRepo;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class BookService {

    @Autowired
    AttachmentRepo attachmentRepo;

    @Autowired
    BookRepo bookRepo;

    BookDto generateBookDto(Book book){
        return new BookDto(book.getId(),book.getName(),book.getAttachment().getId(), book.isActive());
    }

    public ApiResponse getAllOrById(UUID id) {
      if(id!=null){
          Optional<Book> optionalBook = bookRepo.findById(id);
          if (optionalBook.isEmpty()){
              return  new ApiResponse(false,"This book doesn't exist!");
          }
          else {
              return  new ApiResponse(true,"Book found by id!",generateBookDto(optionalBook.get()));
          }
      }
        List<BookDto> collect = bookRepo.getAllByActiveIsTrue().stream().map(this::generateBookDto).collect(Collectors.toList());
        return new ApiResponse(true,"All books!",collect,collect.size());
    }

    public ApiResponse delete(UUID id) {
        try {
            bookRepo.deleteById(id);
        }
        catch (Exception e){
            Optional<Book> optionalBook = bookRepo.findById(id);
            if (optionalBook.isEmpty()) {
                return new ApiResponse(false,"Book is not found with this id!");
            }
            Book book = optionalBook.get();
            book.setActive(false);
            bookRepo.save(book);
            return new ApiResponse(false,"Book has been deactivated!");
        }
        return new ApiResponse(true,"Book has been deleted!");
    }


    public ApiResponse saveorEdit(BookDto dto) {
        String name = dto.getName();
        UUID id = dto.getId();
        Book book = new Book();
        if(bookRepo.existsByName(name)&&id==null){
            return new ApiResponse(false,"Book with this name already exists!");
        }
        else if(id!=null){
            Optional<Book> optionalBook = bookRepo.findById(id);
            if(optionalBook.isEmpty()){
                return new ApiResponse(false,"This book doesn't exist!");
            }
            book = optionalBook.get();
        }
        book.setActive(dto.isActive());
        book.setName(dto.getName());
        book.setAttachment(attachmentRepo.getById(dto.getAttachmentId()));
        bookRepo.save(book);
        return new ApiResponse(true,id!=null?"Book has been edited!":"Book has been saved!");
    }
}
