package uz.flex.english.servise;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.flex.english.entity.Follower;
import uz.flex.english.entity.Notification;
import uz.flex.english.entity.User;
import uz.flex.english.exceptions.PageSizeException;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.interfaseDto.UserInterfaceDto;
import uz.flex.english.repo.FollowerRepo;
import uz.flex.english.repo.NotificationRepo;
import uz.flex.english.repo.UserRepo;
import uz.flex.english.utills.CommandUtills;

import java.util.List;
import java.util.UUID;

@Service
public class FollowService {

    private final FollowerRepo followerRepo;
    private final UserRepo userRepo;
    private final UserService userService;
    private final NotificationRepo notificationRepo;

    public FollowService(FollowerRepo followerRepo, UserRepo userRepo, UserService userService, NotificationRepo notificationRepo) {
        this.followerRepo = followerRepo;
        this.userRepo = userRepo;
        this.userService = userService;
        this.notificationRepo = notificationRepo;
    }


    @Transactional
    public ApiResponse follow(User user, UUID followerId) {
        User follower = userRepo.findById(followerId).orElseThrow(() -> new RuntimeException("User not found"));
        if (follower.getId().equals(user.getId())) {
            return new ApiResponse(false, "You can't follow yourself");
        }
        Follower f = new Follower(user, follower);
        try {
            followerRepo.save(f);
            notificationRepo.save(new Notification("Follow request", follower));
            return new ApiResponse(true, "Followed");
        } catch (Exception e) {
            return new ApiResponse(false, "You are already following this user");
        }
    }

    public ApiResponse unfollow(User user, UUID userId) {
        User follower = userRepo.findById(userId).orElseThrow(() -> new RuntimeException("User not found"));
        if (follower.getId().equals(user.getId())) {
            return new ApiResponse(false, "You can't unfollow yourself");
        }
        boolean b = followerRepo.deleteByUserAndFollowerUser(user, follower);
        return new ApiResponse(b, b ? "Unfollowed" : "Error");
    }

    public ApiResponse getFollowing(UUID userId, int page, int size) throws PageSizeException {
        List<UserInterfaceDto> userPage = userRepo.findByMyFollowing(userId, CommandUtills.descOrAscByCreatedAtPageable(page, size, false));
        return new ApiResponse(true, "ok", userPage,userPage.size());
    }

    public ApiResponse getFollowers(UUID userId, int page, int size) throws PageSizeException {
        List<UserInterfaceDto> userPage = userRepo.findByMyFollowers(userId,CommandUtills.descOrAscByCreatedAtPageable(page, size, false));
        return new ApiResponse(true, "ok", userPage,userPage.size());
    }
}
