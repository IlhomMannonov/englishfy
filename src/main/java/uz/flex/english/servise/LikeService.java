package uz.flex.english.servise;

import org.springframework.stereotype.Service;
import uz.flex.english.entity.*;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.LikeDto;
import uz.flex.english.repo.CommentRepo;
import uz.flex.english.repo.InterestRepo;
import uz.flex.english.repo.LikeRepo;
import uz.flex.english.repo.PostRepo;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class LikeService {
    private final PostRepo postRepo;
    private final CommentRepo commentRepo;
    private final LikeRepo likeRepo;
    private final InterestRepo interestRepo;


    public LikeService(PostRepo postRepo, CommentRepo commentRepo, LikeRepo likeRepo, InterestRepo interestRepo) {
        this.postRepo = postRepo;
        this.commentRepo = commentRepo;
        this.likeRepo = likeRepo;
        this.interestRepo = interestRepo;
    }


    @Transactional
    public ApiResponse likeOrUnLikedPost(User user, LikeDto likeDto) {
        Like like = new Like();
        Post post = postRepo.getById(likeDto.getPostId());
        like.setPost(post);
        like.setUser(user);
        Like dbLike = likeRepo.getByUserAndPost(user, post);
        Interest interest = new Interest();
        List<Tags> tags = post.getTags();
        interest.setUser(user);
        interest.setPost(post);
        if (dbLike != null) {
            likeRepo.delete(dbLike);
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (Tags tag : tags) {
                        interestRepo.removeByUserIdAndTagsId(user.getId(), tag.getId(), post.getId());
                    }
                }
            });
            thread.start();

            return new ApiResponse(true, "Un-Liked");
        } else {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (Tags tag : tags) {
                        interest.setTags(tag);
                        if(!interestRepo.existsByTagsIdAndUserId(tag.getId(),user.getId()))
                         interestRepo.save(interest);
                    }
                }
            });
            thread.start();
            likeRepo.save(like);
            Thread deleteThread = new Thread(new Runnable() {
                @Override
                public void run() {
                 interestRepo.removeOldInterestsByUserId(user.getId());
                }
            });
            try {
                deleteThread.join();
            } catch (InterruptedException e) {}
            deleteThread.start();
            return new ApiResponse(true, "Liked");
        }
    }


    public ApiResponse likeOrUnlikeComment(User user, LikeDto likeDto) {
        Like like = new Like();
        Comment comment = commentRepo.getById(likeDto.getCommentId());
        like.setUser(user);
        like.setComment(comment);
        Like dbLike = likeRepo.getByUserAndComment(user, comment);
        if (dbLike!=null){
            likeRepo.delete(dbLike);
            return new ApiResponse(true, "Un-Liked");
        }else {
            likeRepo.save(like);
            return new ApiResponse(true, "Liked");
        }
    }
}
