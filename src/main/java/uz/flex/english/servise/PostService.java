package uz.flex.english.servise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uz.flex.english.entity.Attachment;
import uz.flex.english.entity.Post;
import uz.flex.english.entity.Tags;
import uz.flex.english.entity.User;
import uz.flex.english.exceptions.PageSizeException;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.PostDto;
import uz.flex.english.payload.TagDto;
import uz.flex.english.payload.interfaseDto.PostInterfaceDto;
import uz.flex.english.repo.AttachmentRepo;
import uz.flex.english.repo.PostRepo;
import uz.flex.english.repo.TagRepo;
import uz.flex.english.utills.CommandUtills;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class PostService {
    @Autowired
    PostRepo postRepo;

    @Autowired
    UserService userService;

    @Autowired
    AttachmentService attachmentService;

    @Autowired
    AttachmentRepo attachmentRepo;

    @Autowired
    TagRepo tagRepo;

    @Autowired
    TagService tagService;

    public ApiResponse save(PostDto postDto, User user) {
        Post post = new Post();
        List<UUID> uuidList = postDto.getTagDtos().stream().map(item -> tagService.getTagDtoId(item)).collect(Collectors.toList());
        List<Tags> tagsList = tagRepo.findAllById(uuidList);
        post.setActive(postDto.isActive());
        List<Attachment> attachments = attachmentRepo.findAllById(postDto.getAttachmentIdList());
        post.setAttachmentList(attachments);
        post.setDescription(postDto.getDescription());
        post.setTags(tagsList);
        post.setUser(user);
        postRepo.save(post);
        return new ApiResponse(true,"Post has been saved!");
    }

    public ApiResponse getAll(UUID id) {
        if(id!=null){
            PostInterfaceDto postInterfaceDto = postRepo.getByPostId(id);
            return new ApiResponse(true,"Post by id",postInterfaceDto);
        }
//        List<PostDto> collect = postRepo.findAll().stream().map(this::generatePostDto).collect(Collectors.toList());
        List<PostInterfaceDto> collect = postRepo.getAllPosts();
        return new ApiResponse(true,"All Posts",collect);
    }

    PostDto generatePostDto(Post post){

        Set<TagDto> tagDtosSet = post.getTags().stream().map(tags -> tagService.generateTagDto(tags)).collect(Collectors.toSet());
        return new PostDto(post.getId(),
                post.getAttachmentList().stream().map(item -> attachmentService.getAttachmentId(item)).collect(Collectors.toList())
                ,post.getDescription(), userService.generateUserDto(post.getUser()),tagDtosSet, post.isActive());
    }

    public ApiResponse getUser(UUID id, boolean all, Integer page, Integer size) {
        if(all){
//            List<PostDto> collect = postRepo.getAllByUserId(id).stream().map(this::generatePostDto).collect(Collectors.toList());
            List<PostInterfaceDto> collect = postRepo.getAllByUser(id);
            return new ApiResponse(true,"All posts of user",collect,collect.size());
        }
        Pageable pageable;
        try {
            pageable = CommandUtills.descOrAscByCreatedAtPageable(page, size, true);
        } catch (PageSizeException e) {
            e.printStackTrace();
            return new ApiResponse(false,e.getMessage());
        }
        Page<PostInterfaceDto> posts = postRepo.getAllPostsPageableByUserId(id,pageable);

//        bu pageable elementini ozini berib yuborish!
//        return new ApiResponse(true,"User posts",posts);

        return new ApiResponse(true,"User posts",posts.getContent(),posts.getTotalElements());
    }

    public ApiResponse getPageable(UUID id,Integer size,Integer page) {
       Pageable pageable;
        try {
           pageable = CommandUtills.simplePageable(page,size);
        } catch (PageSizeException e) {
            return new ApiResponse(false,e.getMessage());
        }
        Page<PostInterfaceDto> interestedPosts = postRepo.getInterestedPosts(id, pageable);
        return new ApiResponse(true,"User interested posts",interestedPosts);
    }
}

