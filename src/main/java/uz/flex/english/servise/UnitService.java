package uz.flex.english.servise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.flex.english.entity.Unit;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.UnitDto;
import uz.flex.english.repo.BookRepo;
import uz.flex.english.repo.UnitRepo;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UnitService {
    @Autowired
    UnitRepo unitRepo;

    @Autowired
    BookRepo bookRepo;

    @Autowired
    BookService bookService;

    public ApiResponse getAllOrById(UUID id) {
        if(id!=null){
            if(unitRepo.existsById(id)){
                return  new ApiResponse(true,"Unit by id!",generateUnitDto(unitRepo.getById(id)));
            }
            else {
                return new ApiResponse(false,"Unit doesn't exist by Id you entered!");
            }
        }
        List<UnitDto> collect = unitRepo.getAllByActiveIsTrue().stream().map(this::generateUnitDto).collect(Collectors.toList());
        return new ApiResponse(true,"All units",collect,collect.size());
    }
    
    UnitDto generateUnitDto(Unit unit){
        return  new UnitDto(unit.getId(),unit.getName(),bookService.generateBookDto(unit.getBook()),unit.getUnitNumber(), unit.isActive());
    }
     
    @Transactional
    public ApiResponse saveOrEdit(UnitDto dto) {
        UUID id = dto.getId();
        Unit unit = new Unit();
        if(unitRepo.existsByNameAndBookId(dto.getName(),dto.getBookDto().getId()))
            return new ApiResponse(false,"Unit already exists with this name!");

        if(id!=null){
            Optional<Unit> optionalUnit = unitRepo.findById(id);
            if(optionalUnit.isEmpty()){
                return  new ApiResponse(false,"This unit doesn't exists!" );
            }
            else {
                unit = optionalUnit.get();
            }
        }
        unit.setUnitNumber(dto.getUnitNumber());
        unit.setName(dto.getName());
        unit.setActive(dto.isActive());
        unit.setBook(bookRepo.getById(dto.getBookDto().getId()));
        unitRepo.save(unit);
        return new ApiResponse(true,id!=null?"Unit has been edited!":"Unit has been saved!");
    }

    
    public ApiResponse delete(UUID id) {
        try {
            unitRepo.deleteById(id);
        }
        catch (Exception e){
            Optional<Unit> optionalUnit = unitRepo.findById(id);
            if (optionalUnit.isEmpty()) {
                return new ApiResponse(false,"Unit is not found with this id!");
            }
            Unit unit = optionalUnit.get();
            unit.setActive(false);
            unitRepo.save(unit);
            return new ApiResponse(false,"Unit has been deactivated!");
        }
        return new ApiResponse(true,"Unit has been deleted!");
    }

    public ApiResponse getByBookId(UUID id) {
        boolean exists = bookRepo.existsById(id);
        if(exists){
            List<UnitDto> collect = unitRepo.getAllByBookIdAndActiveIsTrue(id).stream().map(this::generateUnitDto).collect(Collectors.toList());
            return new ApiResponse(true,"Book units",collect);
        }
        return new ApiResponse(false,"Book doesn't exists you entered!");
    }




}
