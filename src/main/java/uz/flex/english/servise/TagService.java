package uz.flex.english.servise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.flex.english.entity.Tags;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.TagDto;
import uz.flex.english.repo.TagRepo;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TagService {

    @Autowired
    TagRepo tagRepo;

    UUID getTagDtoId(TagDto tagDto){
        return tagDto.getId();
    }

    TagDto generateTagDto(Tags tags){
        return new TagDto(tags.getId(),tags.getName(), tags.isActive());
    }

    public ApiResponse saveOrEdit(TagDto dto) {
        Tags tag = new Tags();
        UUID id = dto.getId();
        if(id!=null){
            Optional<Tags> optionalTags = tagRepo.findById(id);
            if(optionalTags.isEmpty()){
                return new ApiResponse(false,"Tag with this id doesn't exist!");
            }
            tag = optionalTags.get();
        }
        tag.setActive(dto.isActive());
        tag.setName(dto.getName());
        tagRepo.save(tag);
        return new ApiResponse(true,id!=null?"Tag has been edited!":"Tag has been saved!");
    }

    public ApiResponse getAllOrBYId(UUID id) {
        if(id!=null){
            Optional<Tags> optionalTags = tagRepo.findById(id);
            if(optionalTags.isEmpty())
                return new ApiResponse(false,"Tag with this id doesn't exist!");
            else
                return new ApiResponse(true,"Tag with id",generateTagDto(optionalTags.get()));
        }
        List<TagDto> collect = tagRepo.findAll().stream().map(this::generateTagDto).collect(Collectors.toList());
        return new ApiResponse(true,"All tags",collect,collect.size());
    }

    public ApiResponse delete(UUID id) {
        try {
            tagRepo.deleteById(id);
            return new ApiResponse(true,"Tag has been deleted!");
        }
        catch (Exception e){
            Tags tags = tagRepo.getById(id);
            tags.setActive(false);
            tagRepo.save(tags);
            return new ApiResponse(false,"Tag can't be deleted!");
        }
    }
}
