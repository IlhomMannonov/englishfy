package uz.flex.english.servise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.flex.english.entity.Level;
import uz.flex.english.entity.LevelVocabularyAmount;
import uz.flex.english.entity.Word;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.WordDto;
import uz.flex.english.repo.LevelAmountRepo;
import uz.flex.english.repo.LevelRepo;
import uz.flex.english.repo.UnitRepo;
import uz.flex.english.repo.WordRepo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class WordService {
    @Autowired
    LevelAmountRepo levelAmountRepo;

    @Autowired
    WordRepo wordRepo;

    @Autowired
    LevelRepo levelRepo;

    @Autowired
    UnitService unitService;

    @Autowired
    UnitRepo unitRepo;


    public ApiResponse getAllOrById(UUID id) {
        if(id!=null){
            Optional<Word> optionalWord = wordRepo.findById(id);
            if(optionalWord.isEmpty()){
                return new ApiResponse(false,"Word with this id doesn't exist!");
            }
            return new ApiResponse(true,"Word has been found!",generateWordDto(optionalWord.get()));
        }
        List<WordDto> collect = wordRepo.getAllByActiveIsTrue().stream().map(this::generateWordDto).collect(Collectors.toList());
        return new ApiResponse(true,"All words!",collect,collect.size());
    }

    WordDto generateWordDto(Word word){
        return new WordDto(word.getId(),word.getWordUz(),word.getWordEng(),word.getActive(),unitService.generateUnitDto(word.getUnit()));
    }

    public ApiResponse delete(UUID id) {
      try {
          wordRepo.deleteById(id);
      }
      catch (Exception e){
          try {
              Word word = wordRepo.getById(id);
              word.setActive(false);
              wordRepo.save(word);
              return new ApiResponse(true,"Word has been  deactivated!");
          }
          catch (Exception exception){
              return new ApiResponse(false,"Deleting word failed!");
          }
      }
      return new ApiResponse(true,"Word has been deleted!");
    }

    @Transactional
    public ApiResponse save(List<WordDto> dtoList){
        List<Word> words = new ArrayList<>();
        for (WordDto wordDto : dtoList) {
            if(!wordRepo.existsByWordUzAndWordEng(wordDto.getWordUz(),wordDto.getWordEng())){
                Word word = generateWord(wordDto);
                word.setUnit(unitRepo.getById(wordDto.getUnitDto().getId()));
                words.add(word);
            }
        }
        wordRepo.saveAll(words);
        return new ApiResponse(true,"Words have been saved!");
    }

    Word generateWord(WordDto dto){
        Word word =  new Word(dto.getWordUz(),dto.getWordEng(),dto.isActive());
        if (dto.getId()!=null) {
          word.setId(dto.getId());
        }
        return word;
    }

    public ApiResponse getByUnitId(UUID id) {
        if(!wordRepo.existsAllByUnitId(id)){
            return new ApiResponse(false,"Words don't exist according to Unit you entered!");
        }
        List<WordDto> collect = wordRepo.getAllByUnitIdAndActiveTrue(id).stream().map(this::generateWordDto).collect(Collectors.toList());
        return new ApiResponse(true,"All words by Unit ",collect,collect.size());
    }

    public ApiResponse getLevelWords(UUID id){
        Optional<Level> optionalLevel = levelRepo.findById(id);
        if (optionalLevel.isEmpty()) {
            return new ApiResponse(false,"Level with this id doesn't exist!");
        }
        Level level = optionalLevel.get();
        LevelVocabularyAmount amount = levelAmountRepo.getByActiveTrue();
        int page = ((level.getOrdinalNumber()-1)*amount.getLevelAmount());
        List<WordDto> dtoList = wordRepo.getAllByUnitNumber(page + 1, page + amount.getLevelAmount(), amount.getWordAmount())
                .stream()
                .map(this::generateWordDto).collect(Collectors.toList());
        return new ApiResponse(true,"All words of "+level.getName(),dtoList,dtoList.size());
    }

    @Transactional
    public ApiResponse edit(List<WordDto> dtoList) {
        List<Word> words = new ArrayList<>();
        for (WordDto wordDto : dtoList) {
            UUID wordId = wordRepo.getIdByWordUzAndWordEng(wordDto.getWordUz(), wordDto.getWordEng());
            if(wordId!=null&&wordId==wordDto.getId()){
               return new ApiResponse(false,"Words you entered already exists!: "+wordDto.getWordUz()+" and  "+wordDto.getWordEng());
            }
            Word word = wordRepo.getById(wordDto.getId());
            word.setId(wordDto.getId());
            word.setWordUz(word.getWordUz());
            word.setWordEng(word.getWordEng());
            word.setActive(wordDto.isActive());
            word.setUnit(unitRepo.getById(wordDto.getUnitDto().getId()));
            words.add(word);
        }
        wordRepo.saveAll(words);
        return new ApiResponse(true,"Update has been  committed successfully!");
    }

}
