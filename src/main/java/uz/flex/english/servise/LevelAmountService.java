package uz.flex.english.servise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.flex.english.entity.LevelVocabularyAmount;
import uz.flex.english.payload.LevelAmountDto;
import uz.flex.english.repo.LevelAmountRepo;

@Service
public class LevelAmountService {

    @Autowired
    LevelAmountRepo levelAmountRepo;

    LevelAmountDto generateLevelAmountDto(LevelVocabularyAmount amount){
        return new LevelAmountDto(amount.getId(),amount.getLevelAmount(),amount.getWordAmount(),amount.isActive());
    }

}
