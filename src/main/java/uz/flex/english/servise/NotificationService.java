package uz.flex.english.servise;

import org.springframework.stereotype.Service;
import uz.flex.english.entity.Notification;
import uz.flex.english.entity.User;
import uz.flex.english.exceptions.PageSizeException;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.NotificationDto;
import uz.flex.english.repo.NotificationRepo;
import uz.flex.english.repo.UserRepo;
import uz.flex.english.utills.CommandUtills;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class NotificationService {
    private final NotificationRepo notificationRepo;
    private final UserService userService;
    private final UserRepo userRepo;

    public NotificationService(NotificationRepo notificationRepo, UserService userService, UserRepo userRepo) {
        this.notificationRepo = notificationRepo;
        this.userService = userService;
        this.userRepo = userRepo;
    }

    public ApiResponse getMyNotifications(User user, int page, int size) throws PageSizeException {
        List<NotificationDto> collect = notificationRepo.findByUserId(user.getId(), CommandUtills.descOrAscByCreatedAtPageable(page, size, false))
                .stream()
                .map(this::dtoFromNotification).collect(Collectors.toList());
        return new ApiResponse(true, "ok", collect);
    }

    public NotificationDto dtoFromNotification(Notification notification) {
        NotificationDto dto = new NotificationDto();
        dto.setId(notification.getId());
        dto.setUser(userService.dtoFromUser(notification.getUser()));
        return dto;
    }

    public ApiResponse deleteNotification(User user, UUID id) {
        try {
            notificationRepo.deleteByUserAndId(user, id);
            return new ApiResponse(true, "deleted");
        } catch (Exception e) {
            return new ApiResponse(false, "error");
        }
    }

    public ApiResponse deleteAllNotifications(User user) {
        try {
            notificationRepo.deleteByUser(user);
            return new ApiResponse(true, "clear all notifications");
        } catch (Exception e) {
            return new ApiResponse(false, "not clear all notifications");
        }
    }

    public ApiResponse getNotificationNotViewed(User user, int page, int size) throws PageSizeException {
        List<NotificationDto> collect = notificationRepo.findByUserIdAndViewedFalse(user.getId(), CommandUtills.descOrAscByCreatedAtPageable(page, size, false))
                .stream()
                .map(this::dtoFromNotification).collect(Collectors.toList());
        return new ApiResponse(true, "ok", collect);
    }


    public ApiResponse sendAllUsersNotification(String message) {
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    notificationRepo.saveAll(userRepo.findAll()
                            .stream()
                            .map(user -> new Notification(message, user))
                            .collect(Collectors.toList()));
                }
            });
            thread.start();
            return new ApiResponse(true, "send all users notification");
        } catch (Exception e) {
            return new ApiResponse(false, "not send all users notification");
        }
    }
}
