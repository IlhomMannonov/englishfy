package uz.flex.english.servise;

import org.springframework.stereotype.Service;
import uz.flex.english.entity.User;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.interfaseDto.MySolvedTestAnswerDto;
import uz.flex.english.payload.interfaseDto.MySolvedTestDto;
import uz.flex.english.repo.SolvedTestAnswerRepo;
import uz.flex.english.repo.SolvedTestRepo;

import java.util.List;
import java.util.UUID;

@Service
public class SolvedTestService {

    private final SolvedTestRepo solvedTestRepo;
    private final SolvedTestAnswerRepo solvedTestAnswerRepo;

    public SolvedTestService(SolvedTestRepo solvedTestRepo, SolvedTestAnswerRepo solvedTestAnswerRepo) {
        this.solvedTestRepo = solvedTestRepo;
        this.solvedTestAnswerRepo = solvedTestAnswerRepo;
    }

    public ApiResponse getMySolvedTest(UUID id) {
        List<MySolvedTestDto> history = solvedTestRepo.getByUserIdMySolvedTest(id);
        return new ApiResponse(true, "ok", history);
    }

    public ApiResponse getMySolvedOneTest(UUID id, UUID userId) {
        List<MySolvedTestAnswerDto> answer = solvedTestAnswerRepo.getSolvedTestAnswerBySolvedTestAndUserId(id, userId);
        return new ApiResponse(true, "ok", answer);
    }
}
