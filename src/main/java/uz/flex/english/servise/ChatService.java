package uz.flex.english.servise;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.flex.english.entity.*;
import uz.flex.english.exceptions.PageSizeException;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.DirectDto;
import uz.flex.english.payload.MessageDto;
import uz.flex.english.payload.interfaseDto.DirectInterfaceDto;
import uz.flex.english.payload.interfaseDto.MessageInterfaceDto;
import uz.flex.english.repo.*;
import uz.flex.english.utills.CommandUtills;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
public class ChatService {
    private final UserRepo userRepo;
    private final DirectRepo directRepo;
    private final MessageRepo messageRepo;
    private final GroupRepo groupRepo;
    private final AttachmentRepo attachmentRepo;


    protected Direct directFromDto(DirectDto dto) {
        Direct direct = new Direct();
        direct.setId(dto.getId());
        direct.setToUser(userRepo.getById(dto.getToUser().getId()));
        direct.setActive(dto.isActive());
        direct.setGroup(groupRepo.getById(dto.getGroupId()));
        return direct;
    }

    @Transactional
    public ApiResponse sendMessage(MessageDto messageDto, User user, UUID userId) {
        Direct direct = new Direct();
        Message message = messageFromDto(messageDto);
        message.setUser(user);
        if (directRepo.existsByFromUserIdAndToUserId(user.getId(), userId)) {
            direct = directRepo.getByFromUserIdAndToUserId(user.getId(), userId);
        } else if (directRepo.existsByFromUserIdAndToUserId(userId, user.getId())) {
            direct = directRepo.getByFromUserIdAndToUserId(userId, user.getId());
        } else {

            direct.setActive(true);
            direct.setFromUser(user);
            direct.setToUser(userRepo.getById(userId));
            directRepo.save(direct);
        }
        message.setDirect(direct);
        if (messageDto.getAttachmentIds() != null) {
            messageDto.getAttachmentIds().forEach(id -> {
                message.getAttachmentList().add(attachmentRepo.getById(id));
            });
        }

        messageRepo.save(message);
        return new ApiResponse(true, "Message sent", direct.getId());
    }


    public ApiResponse sendMessageToGroup(MessageDto messageDto, User user, UUID groupId) {
        Group group = groupRepo.getById(groupId);
        Direct direct = new Direct();
        Message message = messageFromDto(messageDto);
        message.setUser(user);

        if (directRepo.existsByFromUserAndGroupId(user, groupId)) {
            direct = directRepo.getByFromUserAndGroupId(user, groupId);
        } else {
            direct.setGroup(group);
            direct.setActive(true);
            direct.setFromUser(user);
            directRepo.save(direct);
        }
        if (messageDto.getAttachmentIds() != null) {
            for (UUID attachmentId : messageDto.getAttachmentIds()) {
                message.getAttachmentList().add(attachmentRepo.getById(attachmentId));
            }
        }
        message.setDirect(direct);
        messageRepo.save(message);
        return new ApiResponse(true, "Message sent", direct.getId());

    }

    protected Message messageFromDto(MessageDto dto) {
        Message message = new Message();
        message.setId(dto.getId());
        message.setText(dto.getText());
        if (dto.getMessageId() != null)
            message.setMessage(messageRepo.getById(dto.getMessageId()));
        if (dto.getDirectId() != null) {
            message.setDirect(directRepo.getById(dto.getDirectId()));
        }
        return message;
    }


    public ApiResponse updateMessage(MessageDto messageDto, User user) {
        Message message = messageRepo.getById(messageDto.getId());
        if (message.getUser().getId().equals(user.getId())) {
            message.setText(messageDto.getText());
            message.setEdited(true);
            messageRepo.save(message);
            return new ApiResponse(true, "Message updated");
        } else {
            return new ApiResponse(false, "You can't update this message");
        }
    }

    public ApiResponse getMessagesUserToUser(UUID directId, User user, int page, int size) throws PageSizeException {
        Page<MessageInterfaceDto> messages = messageRepo.findAllByDirectChatId(directId, user.getId(), CommandUtills.descOrAscByCreatedAtPageable(page, size, false));
        return new ApiResponse(true, "Messages", messages.getContent(), messages.getTotalElements());
    }

    public ApiResponse getMyDirects(User user, int page, int size) throws PageSizeException {
        List<DirectInterfaceDto> allByMyNetwork = directRepo.findAllByMyNetwork(user.getId(), page, size);
        return new ApiResponse(true, "Directs", allByMyNetwork, allByMyNetwork.size());
    }

    public ApiResponse getAllMessagesByGroupId(UUID groupId, int page, int size, User user) throws PageSizeException {
        Page<MessageInterfaceDto> messages = messageRepo.findAllByGroupId(groupId, user.getId(), CommandUtills.descOrAscByCreatedAtPageable(page, size, false));
        return new ApiResponse(true, "Messages", messages.getContent(), messages.getTotalElements());
    }
}
