package uz.flex.english.servise;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.flex.english.entity.User;
import uz.flex.english.entity.enums.RoleName;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.LoginDto;
import uz.flex.english.payload.ResToken;
import uz.flex.english.payload.SingUpDto;
import uz.flex.english.repo.RoleRepo;
import uz.flex.english.repo.UserRepo;
import uz.flex.english.secret.JwtProvider;
import uz.flex.english.utills.CommandUtills;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

@Service
public class AuthServise {

    @Autowired
    UserRepo userRepo;

    @Autowired
    AuthenticationManager manager;

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    RoleRepo roleRepo;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    MailService mailService;


    public ResToken login(LoginDto loginDto) {
        Authentication authenticate = manager.authenticate(new UsernamePasswordAuthenticationToken(
                loginDto.getUsernameOrEmail(),
                loginDto.getPassword()
        ));

        User user = (User) authenticate.getPrincipal();

        return new ResToken(jwtProvider.generateToken(user));
    }

    public ApiResponse register(SingUpDto dto) {
        if (!CommandUtills.usernameValid(dto.getUsername()))
            return new ApiResponse(false, "forbidden username");
        boolean exists = userRepo.existsByUsername(dto.getUsername());
        boolean email = userRepo.existsByEmail(dto.getEmail());
        if(!exists&&!email){
            if(dto.getPassword().equals(dto.getPrePassword())){
                User user =new User(dto.getUsername(), dto.getEmail(),passwordEncoder.encode(dto.getPassword()),
                        0,new HashSet<>(Collections.singletonList(roleRepo.findByRoleName(RoleName.ROLE_CUSTOMER))));
                User savedUser = userRepo.save(user);
                System.out.println(savedUser);

                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mailService.sendHTML(savedUser);
                    }
                });
                thread.start();
                return new ApiResponse(true, "Successfully registered! " +
                        "We send a verification message to your mail. Please verify!");
            }
            return new ApiResponse(false, "Your password and prepassword did not match!");
        } else if (email) {
            return new ApiResponse(false, "Registration failed!" +
                    "This email already exists!");
        }

        return new ApiResponse(false, "Registration failed!" +
                "This username already exists!");
    }


    public ApiResponse forgot(LoginDto loginDto) {
        User user = userRepo.findByUsernameOrEmail(loginDto.getUsernameOrEmail(), loginDto.getUsernameOrEmail());
        if (user == null)
            return new ApiResponse(false, "Not found username or email");

        if (loginDto.getPassword() != null) {
            if (user.getEmailVerifyCode().equals(loginDto.getVerifyCode())) {
                user.setPassword(passwordEncoder.encode(loginDto.getPassword()));
                user.setEmailVerifyCode(null);
                userRepo.save(user);
                return new ApiResponse(true, "Successfully updated password");
            }
        } else {

            int random = (int) ((Math.random() * (9999 - 1000)) + 1000);
            user.setEmailVerifyCode("" + random);
            Thread thread = new Thread(new Runnable() {
                @SneakyThrows
                @Override
                public void run() {
                    userRepo.save(user);
                    mailService.sendMessage(user.getEmail(), "Coniform password", "" + random);
                }
            });
            thread.start();
        }
        return new ApiResponse(true, "ok");
    }
}
