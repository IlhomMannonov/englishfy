package uz.flex.english.servise;

import org.springframework.stereotype.Service;
import uz.flex.english.entity.*;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.TenseDto;
import uz.flex.english.payload.interfaseDto.MySolvedTensesDto;
import uz.flex.english.repo.SolvedAllTensePartRepo;
import uz.flex.english.repo.SolvedTenseRepo;
import uz.flex.english.repo.TenseRepo;

import java.util.List;
import java.util.UUID;

@Service
public class TensesHistoryService {
    private final SolvedTenseRepo solvedTenseRepo;
    private final SolvedAllTensePartRepo solvedAllTensePartRepo;
    private final TenseService tenseService;
    private final TenseRepo tenseRepo;

    public TensesHistoryService(SolvedTenseRepo solvedTenseRepo, SolvedAllTensePartRepo solvedAllTensePartRepo, TenseService tenseService, TenseRepo tenseRepo) {
        this.solvedTenseRepo = solvedTenseRepo;
        this.solvedAllTensePartRepo = solvedAllTensePartRepo;
        this.tenseService = tenseService;
        this.tenseRepo = tenseRepo;
    }

    public ApiResponse MyExamResult(User user) {
        List<MySolvedTensesDto> solvedTensesDtoList = solvedTenseRepo.findMySolvedTensesByUserId(user.getId());
        return new ApiResponse(true, "ok", solvedTensesDtoList);
    }

    public ApiResponse userExam(UUID id) {
        List<MySolvedTensesDto> solvedTensesDtoList = solvedTenseRepo.findMySolvedTensesByUserId(id);
        return new ApiResponse(true, "ok", solvedTensesDtoList);
    }

    public ApiResponse getMyTensesAnswer(User user, UUID id) {
        SolvedTense solvedTense = solvedTenseRepo.getByUserIdAndId(user.getId(), id);
        Tense tense = tenseRepo.getById(solvedTense.getTense().getId());
        for (AllTense allTens : tense.getAllTenses()) {
            for (AllTensePart allTensePart : allTens.getAllTensesPartList()) {
                if (allTensePart.getKey()) {
                    String inputWord = solvedAllTensePartRepo.findBySolvedAllTenseIdAndAllTensesPartId(solvedTense.getId(), allTensePart.getId());
                    allTensePart.setText(inputWord);
                }
            }
        }

        TenseDto tenseDto = tenseService.dtoFromTense(tense);
        return new ApiResponse(true, "ok", tenseDto);
    }

    public ApiResponse getUserTensesAnswer(UUID tenseId, UUID userId) {
        SolvedTense solvedTense = solvedTenseRepo.getByUserIdAndId(userId,tenseId);
        Tense tense = tenseRepo.getById(solvedTense.getTense().getId());

        for (AllTense allTens : tense.getAllTenses()) {
            for (AllTensePart allTensePart : allTens.getAllTensesPartList()) {
                if (allTensePart.getKey()) {
                    String inputWord = solvedAllTensePartRepo.findBySolvedAllTenseIdAndAllTensesPartId(solvedTense.getId(), allTensePart.getId());
                    allTensePart.setText(inputWord);
                }
            }
        }

        TenseDto tenseDto = tenseService.dtoFromTense(tense);
        return new ApiResponse(true, "ok", tenseDto);
    }
}
