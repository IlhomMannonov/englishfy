package uz.flex.english.servise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.flex.english.entity.Level;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.LevelDto;
import uz.flex.english.repo.LevelRepo;


import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class LevelService {
    @Autowired
    LevelRepo levelRepo;

    //hamma  Levelni isActivi ture bulganlarni oladi!
    public ApiResponse getAllLevelTrue() {
        List<LevelDto> collect = levelRepo.findAllByActiveIsTrue()
                .stream()
                .map(this::dtoFromLevel)
                .collect(Collectors.toList());

        return new ApiResponse(true, "ok", collect);
    }

    // levelni id orqali olish!
    public ApiResponse getOne(UUID id) {
        Optional<Level> optionalLevel = levelRepo.findByIdAndActiveIsTrue(id);
        if (optionalLevel.isEmpty()) {
            return new ApiResponse(false, "not found");
        }
        return new ApiResponse(true, "ok", dtoFromLevel(optionalLevel.get()));
    }

    // LevelDto  da agar id kelsa Levelni edit qilamiz id siz kelsa  add qilamiz!
    public ApiResponse addLevel(LevelDto levelDto) {
        if (levelDto.getId() == null) {
            Level level = new Level();
            level.setName(levelDto.getName());
            level.setActive(levelDto.isActive());
            levelRepo.save(level);
            return new ApiResponse(true, "level added");
        } else {
            Optional<Level> optionalLevel = levelRepo.findById(levelDto.getId());
            if (optionalLevel.isPresent()) {
                Level level = optionalLevel.get();
                level.setName(levelDto.getName());
                level.setActive(levelDto.isActive());
                Level save = levelRepo.save(level);
                return new ApiResponse(true, "Level edited", save);
            }
            return new ApiResponse(false, "not found");
        }
    }

    // Level ni id orqali uchirish
    public ApiResponse delete(UUID id) {

        Optional<Level> optionalLevel = levelRepo.findById(id);
        if (optionalLevel.isPresent()) {
            Level level = optionalLevel.get();
            levelRepo.delete(level);
            return new ApiResponse(true, "Level deleted!");
        }
        return new ApiResponse(false, "Level not found");
    }

    // bu levelni dtoga urab qaytaradi
    public LevelDto dtoFromLevel(Level level) {
        return new LevelDto(level.getId(), level.getName(), level.isActive(), level.getOrdinalNumber());
    }

    LevelDto generateLevelDto(Level level) {
        return new LevelDto(level.getId(), level.getName(), level.isActive(), level.getOrdinalNumber());
    }

    // bu method hamma levellarni qaytaradi
    public ApiResponse getAllLeveTrueOrFalse() {
        List<LevelDto> collect = levelRepo.findAll().stream().map(this::dtoFromLevel).collect(Collectors.toList());
        return new ApiResponse(true, "ok", collect);
    }

}