package uz.flex.english.servise;


import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.flex.english.entity.Attachment;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.repo.AttachmentRepo;
import uz.flex.english.repo.ContentTypeRepo;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Service
public class AttachmentService {

    @Value("${file.path}")
    private String FILE_URL;

    private final AttachmentRepo attachmentRepo;
    private final ContentTypeRepo contentTypeRepo;

    public AttachmentService(AttachmentRepo attachmentRepo, ContentTypeRepo contentTypeRepo) {
        this.attachmentRepo = attachmentRepo;
        this.contentTypeRepo = contentTypeRepo;
    }

    @Transactional
    public ApiResponse upload(MultipartHttpServletRequest request) throws IOException {
        Iterator<String> fileNames = request.getFileNames();
        List<String> ids = new ArrayList<>();
        while (fileNames.hasNext()) {
            MultipartFile file = request.getFile(fileNames.next());
            if (file != null) {
                if (!validateFile(file))
                    return new ApiResponse(false, "forbidden file");
                String originalFilename = file.getOriginalFilename();
                String[] split = originalFilename.split(".");
                String name = UUID.randomUUID() + "." + split[split.length - 1];
                Attachment attachment = new Attachment();
                attachment.setContentType(file.getContentType());
                attachment.setFileSize(file.getSize());
                attachment.setContentType(file.getContentType());
                attachment.setName(name);
                attachment.setFileUrl(FILE_URL);
                Attachment saved = attachmentRepo.save(attachment);
                Path path = Paths.get(FILE_URL + "/" + name);
                Files.copy(file.getInputStream(), path);
                ids.add(saved.getId()+":"+file.getContentType());
            }
        }
        return new ApiResponse(true, "ok", ids);
    }
    public UUID getAttachmentId(Attachment attachment){
        return attachment.getId();
    }

    public boolean validateFile(MultipartFile file) {
        String[] split = file.getContentType().split("\\.");
        String contentType = split[split.length - 1];
        System.out.println(contentType);
        return contentTypeRepo.existsByName(contentType);
    }

    public ResponseEntity<?> getById(UUID id) {
        Attachment attachment = attachmentRepo.getById(id);
        File file = new File(attachment.getContentType());
        try {
            byte[] bytes = Files.readAllBytes(file.toPath());
            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(attachment.getContentType()))
                    .header(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\""+attachment.getFileOriginalName()+"\"")
                    .body(bytes);
        } catch (IOException e) {
            return ResponseEntity.status(409).body(e.getMessage());
        }
    }
}
