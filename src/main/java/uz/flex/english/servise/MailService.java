package uz.flex.english.servise;

import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import uz.flex.english.entity.User;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.repo.UserRepo;
import uz.flex.english.secret.JwtProvider;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Service
public class MailService {
    @Autowired
    JavaMailSender mailSender;

    @Autowired
    UserRepo userRepo;

    @Autowired
    Configuration configuration;

    @Autowired
    JwtProvider provider;


    public ApiResponse sendHTML(User user) {
        try {
            String token = provider.generateToken(user);
            Map<String, Object> model = new HashMap<>();
            model.put("token", token);
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
            Template template = configuration.getTemplate("email-template.ftl");
            String htmlPage = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
            helper.setTo(user.getEmail());
            //TODO create a name for application
            helper.setSubject("Verification message from Our Application");
            helper.setText(htmlPage, true);
            mailSender.send(mimeMessage);
            return new ApiResponse(true,
                    "Message has been sent!");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse(false, "Error in sending html message");
    }


    public ApiResponse activate(String token, Boolean active) {
        boolean correct = provider.validateToken(token);
        if (correct) {
            User user = provider.getUserFromToken(token);
            if (active) {
                user.setEnabled(active);
                userRepo.save(user);
                return new ApiResponse(true, "Successfully activated!");
            } else {
                if (!user.isEnabled())
                    userRepo.delete(user);
            }
            return new ApiResponse(false, "User sent a rejection message!");
        }
        return new ApiResponse(false, "Token is not correct!");
    }

    public Boolean sendMessage(String email, String subject, String text) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

        helper.setSubject(subject);
        helper.setTo(email);
        helper.setTo(text);
        try {
        mailSender.send(mimeMessage);
        return true;
        }catch (Exception e){
            return false;
        }

    }

    //Remov
}
