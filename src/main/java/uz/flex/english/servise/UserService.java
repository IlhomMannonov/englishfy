package uz.flex.english.servise;


import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import uz.flex.english.entity.User;
import uz.flex.english.entity.enums.CoinType;
import uz.flex.english.exceptions.PageSizeException;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.PasswordDto;
import uz.flex.english.payload.UserDto;
import uz.flex.english.payload.interfaseDto.UserInterfaceDto;
import uz.flex.english.repo.CoinValueRepo;
import uz.flex.english.repo.UserRepo;
import uz.flex.english.utills.CommandUtills;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class UserService {
    private final UserRepo userRepo;
    private final CoinValueRepo coinValueRepo;
    private final AuthenticationManager manager;

    public UserService(UserRepo userRepo, CoinValueRepo coinValue, AuthenticationManager manager) {
        this.userRepo = userRepo;
        this.coinValueRepo = coinValue;
        this.manager = manager;
    }

    /**
     * user ozi profilini edit qilishi mumkin agar username o'zgartiriladigan bolsa undan keraklicha coin yechub olinandi
     */
    public ApiResponse edit(User user, UserDto dto) {

        user.setId(dto.getId());
        user.setName(dto.getName());
        user.setLastName(dto.getLastName());
        user.setPhoneNumber(dto.getPhoneNumber());

        if (!user.getUsername().equals(dto.getUserName())) {

            if (!CommandUtills.usernameValid(dto.getUserName()))
                return new ApiResponse(false, "Forbidden username");


            user.setUsername(dto.getUserName());
            long amount = coinValueRepo.getCoinValueByCoinType(CoinType.USERNAME).getAmount();
            if (user.getCoin() - amount < 0)
                return new ApiResponse(false, "your coins are not enough for change username");
            else
                user.setCoin(user.getCoin() - amount);
        }
        try {
            userRepo.save(user);
            return new ApiResponse(true, "profile has been successfully modified");
        } catch (Exception e) {
            return new ApiResponse(false, "Something wrong");
        }

    }

    List<UserDto> userDtoList = new ArrayList<>();

    public ApiResponse getALl(int page, int size, boolean all) throws PageSizeException {
        if (all) {
            userDtoList = userRepo.findAll().stream().map(this::dtoFromUser).collect(Collectors.toList());
        } else {
            userDtoList = userRepo.findAll(CommandUtills.descOrAscByCreatedAtPageable(page, size, false))
                    .stream()
                    .map(this::dtoFromUser)
                    .collect(Collectors.toList());
        }
        return new ApiResponse(true, "ok", userDtoList);
    }

    public UserDto dtoFromUser(User user) {
        return new UserDto(
                user.getId(),
                user.getCreatedat(),
                user.getName(),
                user.getPhoneNumber(),
                user.getUsername(),
                user.getLastName(),
                user.getEmail(),
                user.getPhotoUrl(),
                user.getBio(),
                user.isEnabled(),
                user.getCoin()
        );
    }

    public ApiResponse block(UUID id, boolean enable) {
        Optional<User> userOptional = userRepo.findById(id);
        if (userOptional.isEmpty())
            return new ApiResponse(false, "User not fount");

        User user = userOptional.get();

        try {
            if (user.isEnabled() != enable) {
                user.setEnabled(enable);
                userRepo.save(user);
            }
            return new ApiResponse(true, enable ? "User un-blocked" : "User blocked");
        } catch (Exception e) {
            return new ApiResponse(true, enable ? "User not un-blocked" : "User not blocked");

        }
    }

    public ApiResponse deleteUser(UUID id) {
        try {
            userRepo.deleteById(id);
            return new ApiResponse(true, "User deleted");
        } catch (Exception e) {
            return new ApiResponse(false, "User not deleted");
        }
    }

    public UserDto generateUserDto(User user){
        return new UserDto(user.getId(),user.getName(),user.getUsername(), user.getLastName());
    }

    public ApiResponse passwordEdit(PasswordDto dto, User user) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        boolean matches = passwordEncoder.matches(dto.getOldPassword(), user.getPassword());
        if (!matches)
            return new ApiResponse(false, "Wrong old password");

        if (!dto.getPrePassword().equals(dto.getNewPassword()))
            return new ApiResponse(false, "Passwords are not compatible");


        user.setPassword(passwordEncoder.encode(dto.getNewPassword()));

        try {
            if (!passwordEncoder.matches(dto.getNewPassword(), user.getPassword()))
                userRepo.save(user);
            return new ApiResponse(true, "Password updated");
        } catch (Exception e) {
            return new ApiResponse(false, "Something wrong");
        }
    }


    public ApiResponse getMe(User user) {


        return new ApiResponse(true, "ok", dtoFromUser(user));
    }

    public ApiResponse getUserById(UUID id) {
        Optional<User> userOptional = userRepo.findById(id);
        if (userOptional.isEmpty())
            return new ApiResponse(false, "User not found");

        UserDto userDto = dtoFromUser(userOptional.get());
        return new ApiResponse(true, "ok", userDto);
    }

    public ApiResponse getUserByIdForUser(UUID id, User user) {
        UserInterfaceDto userInterfaceDto = userRepo.findByUserId(id, user.getId());
        return new ApiResponse(true, "ok", userInterfaceDto);
    }

    public ApiResponse getRecommendUser(UUID id) {
        List<UserInterfaceDto> userInterfaceDtoList = userRepo.findRecommendUser(id);
        return new ApiResponse(true, "ok", userInterfaceDtoList);
    }

    public ApiResponse searchUserByUsername(String username) {
        List<UserInterfaceDto> userInterfaceDtoList = userRepo.findByUsername(username);
        return new ApiResponse(true, "ok", userInterfaceDtoList);
    }
}
