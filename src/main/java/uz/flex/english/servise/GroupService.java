package uz.flex.english.servise;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.flex.english.entity.Group;
import uz.flex.english.exceptions.PageSizeException;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.GroupDto;
import uz.flex.english.payload.interfaseDto.GroupInterfaceDto;
import uz.flex.english.repo.GroupRepo;
import uz.flex.english.repo.LevelRepo;
import uz.flex.english.utills.CommandUtills;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class GroupService {
    private final GroupRepo groupRepo;
    private final LevelRepo levelRepo;
    private final LevelService levelService;


    // save and edit group
    public ApiResponse add(GroupDto groupDto) {
        Group group = groupFromDto(groupDto);
        try {
            groupRepo.save(group);
            return new ApiResponse(true, groupDto.getId() != null ? "Group edited" : "Group added");
        } catch (Exception e) {
            return new ApiResponse(false, groupDto.getId() != null ? "Group not edited" : "Group not added");
        }
    }


    // see all groups with pagination
    public ApiResponse getAll(int page, int size) throws PageSizeException {
        Page<GroupDto> collect = groupRepo.findAll(CommandUtills.descOrAscByCreatedAtPageable(page, size, false)).map(this::dtoFromGroup);
        return new ApiResponse(true, "", collect.getContent(), collect.getTotalElements());
    }


    // generate group from dto
    protected Group groupFromDto(GroupDto groupDto) {
        Group group = new Group();
        if (groupDto.getId() != null) {
            group = groupRepo.findById(groupDto.getId()).orElse(new Group());
            group.setId(groupDto.getId());
        }
        group.setName(groupDto.getName());
        if (groupDto.getLevelDto().getId() != null) {
            group.setLevel(levelRepo.getById(groupDto.getLevelDto().getId()));
        }
        return group;
    }

    // generate dto from group
    protected GroupDto dtoFromGroup(Group group) {
        GroupDto groupDto = new GroupDto();
        groupDto.setId(group.getId());
        groupDto.setName(group.getName());
        if (group.getLevel() != null) {
            groupDto.setLevelDto(levelService.generateLevelDto(group.getLevel()));
        }
        return groupDto;
    }


    public ApiResponse delete(UUID id) {
        try {
            groupRepo.deleteById(id);
            return new ApiResponse(true, "Group deleted");
        } catch (Exception e) {
            return new ApiResponse(false, "Group not deleted");
        }
    }

    public ApiResponse getByLevelId(UUID levelId, int page, int size) throws PageSizeException {

        Page<GroupInterfaceDto> collect = groupRepo.getAllByLevelId(levelId, CommandUtills.descOrAscByCreatedAtPageable(page, size, false));
        return new ApiResponse(true, "", collect.getContent(), collect.getTotalElements());

    }
}
