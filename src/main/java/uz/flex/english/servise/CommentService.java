package uz.flex.english.servise;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import uz.flex.english.entity.Comment;
import uz.flex.english.entity.User;
import uz.flex.english.exceptions.PageSizeException;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.CommentDto;
import uz.flex.english.payload.interfaseDto.CommentInterfaceDto;
import uz.flex.english.repo.CommentRepo;
import uz.flex.english.repo.PostRepo;
import uz.flex.english.utills.CommandUtills;

import java.util.UUID;

@Service
public class CommentService {
    private final CommentRepo commentRepo;
    private final PostRepo postRepo;

    public CommentService(CommentRepo commentRepo, PostRepo postRepo) {
        this.commentRepo = commentRepo;
        this.postRepo = postRepo;
    }


    public ApiResponse addComment(User user, CommentDto commentDto) {
        Comment comment = commentFromDto(commentDto, user);
        try {
            commentRepo.save(comment);
            return new ApiResponse(true, "Comment added");
        } catch (Exception e) {
            return new ApiResponse(false, "Comment not added");
        }
    }

    public Comment commentFromDto(CommentDto dto, User user) {
        Comment comment = new Comment();
        comment.setText(dto.getText());
        comment.setUser(user);
        comment.setPost(postRepo.getById(dto.getPostId()));
        return comment;
    }

    public ApiResponse deleteComment(User user, UUID id) {
        Comment comment = commentRepo.getById(id);
        if (comment.getUser().getId().equals(user.getId())) {
            try {
                commentRepo.delete(comment);
                return new ApiResponse(true, "Comment deleted");
            } catch (Exception e) {
                return new ApiResponse(false, "Comment not deleted");
            }
        } else {
            return new ApiResponse(false, "You can not delete this comment");
        }
    }


    // to be continued
    public ApiResponse getAllCommentByPostId(UUID id, int page, int size) throws PageSizeException {

        Page<CommentInterfaceDto> dtoPage = commentRepo.findByPostId(id, CommandUtills.descOrAscByCreatedAtPageable(page, size, false));
        System.out.println(dtoPage.getContent());
        return new ApiResponse(true, "Comment list", dtoPage.getContent());
    }
}
