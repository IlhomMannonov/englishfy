package uz.flex.english.servise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.flex.english.entity.*;
import uz.flex.english.entity.enums.CoinType;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.ExamResponse;
import uz.flex.english.payload.SolvedWordHistoryDto;
import uz.flex.english.payload.SolvedWordPartDto;
import uz.flex.english.repo.*;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class SolveWordService {
    @Autowired
    UnitRepo unitRepo;
    @Autowired
    WordRepo wordRepo;
    @Autowired
    WordService wordService;
    @Autowired
    SolvedWordHistoryRepo historyRepo;
    @Autowired
    UserRepo userRepo;
    @Autowired
    UserService userService;
    @Autowired
    CoinValueRepo coinValueRepo;
    @Autowired
    UnitService unitService;



    @Transactional
    public ApiResponse save(List<SolvedWordPartDto> solvedWordDtos, User user) {
        SolvedWordHistory history = new SolvedWordHistory();
        CoinValue coin = coinValueRepo.getCoinValueByCoinType(CoinType.WORD);
        int coinAmount=0;
        List<SolvedWordPart> wordParts = new ArrayList<>();
        int rightAnswers=0;
        for (SolvedWordPartDto dto : solvedWordDtos){
            Word word = wordRepo.getById(dto.getWordDto().getId());
            boolean equals = word.getWordEng().equals(dto.getInputWord());
            wordParts.add(new SolvedWordPart(dto.getInputWord(),
                    word,
                    history,
                    equals
                    )
            );
            if(equals)
                coinAmount+=coin.getAmount();
            rightAnswers++;
             }

        UUID unitId = solvedWordDtos.get(0).getWordDto().getUnitDto().getId();
        if(!historyRepo.existsByUserIdAndUnitId(user.getId(),unitId)){
            user.setCoin(user.getCoin()+coinAmount);
        }
        history.setUnit(unitRepo.getById(unitId));
        history.setCoinValue(coin);
        history.setWordParts(wordParts);
        history.setUser(user);
        historyRepo.save(history);
        ExamResponse response = new ExamResponse(solvedWordDtos.size(), rightAnswers,  (double) (rightAnswers * 100) / solvedWordDtos.size());
        return new ApiResponse(true, "User result has been saved!");
    }

    public ApiResponse getAllOrById(UUID id) {
        if (id != null) {
            Optional<SolvedWordHistory> optionalHistory = historyRepo.findById(id);
            if (optionalHistory.isEmpty())
                return new ApiResponse(false, "Solved word history doesn't exist with this id!");
            else
                return new ApiResponse(true, "Solved word history with  id!", generateWordHistoryDto(optionalHistory.get()));
        }
        List<SolvedWordHistoryDto> collect = historyRepo.findAll().stream().map(this::generateWordHistoryDto).collect(Collectors.toList());
        return new ApiResponse(true, "All solved word histories", collect, collect.size());
    }

    SolvedWordPartDto generateSolvedWordPartDto(SolvedWordPart wordPart) {
        return new SolvedWordPartDto(wordPart.getId(),wordService.generateWordDto(wordPart.getWord()),wordPart.getInputWord(), wordPart.isCorrect());
    }

    SolvedWordHistoryDto generateWordHistoryDto(SolvedWordHistory history) {
        List<SolvedWordPartDto> collect = history.getWordParts().stream().map(this::generateSolvedWordPartDto).collect(Collectors.toList());
        return new SolvedWordHistoryDto(history.getId(), unitService.generateUnitDto(history.getUnit()),userService.generateUserDto(history.getUser()), history.getCoinValue(), collect);
    }

    public ApiResponse getByUserAndUnit(UUID unitId, UUID userId) {
        Optional<List<SolvedWordHistory>> optionalList = historyRepo.getAllByUnitIdAndUserId(unitId,userId);
        if (optionalList.isEmpty()) {
            return new ApiResponse(false, "User id or Unit id doesn't exist!");
        }
        List<SolvedWordHistoryDto> collect = optionalList.get().stream().map(this::generateWordHistoryDto).collect(Collectors.toList());
        return new ApiResponse(true, "All histories by user id and unit id", collect, collect.size());
    }

    public ApiResponse checkWord(SolvedWordPartDto wordPartDto) {
        Optional<Word> optionalWord = wordRepo.findById(wordPartDto.getWordDto().getId());
        if (optionalWord.isEmpty())
            return new ApiResponse(false,"Word is not found with this Id!");
        return new ApiResponse(true,optionalWord.get().getWordEng().equals(wordPartDto.getInputWord())?"true":"false");
    }

    public ApiResponse getByUser(UUID userId) {
        Optional<List<SolvedWordHistory>> optionalHistory = historyRepo.getAllByUserId(userId);
        if (optionalHistory.isEmpty()) {
            return new ApiResponse(false,"Histories with this user id don't exist!");
        }
        List<SolvedWordHistoryDto> collect = optionalHistory.get().stream().map(this::generateWordHistoryDto).collect(Collectors.toList());
        return new ApiResponse(true, "All histories by user id", collect, collect.size());
    }


}












