package uz.flex.english.servise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.flex.english.entity.TenseCategory;
import uz.flex.english.payload.ApiResponse;
import uz.flex.english.payload.TensesCategoryDto;
import uz.flex.english.repo.LevelRepo;
import uz.flex.english.repo.TenseCategoryRepo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TenseCategoryService {

    @Autowired
    TenseCategoryRepo tensesCategoryRepo;

    @Autowired
    LevelRepo levelRepo;

    public ApiResponse addTenses(TensesCategoryDto tensesCategoryDto) {
        TenseCategory tense = new TenseCategory();
        try {

            if (tensesCategoryDto.getId() != null)
                tense = tensesCategoryRepo.getById(tensesCategoryDto.getId());

            if (tensesCategoryDto.getTenseCategoryId() != null)
                tense.setTenseCategory(tensesCategoryRepo.getById(tensesCategoryDto.getTenseCategoryId()));

            tense.setLevel(levelRepo.getById(tensesCategoryDto.getLevelId()));
            tense.setName(tensesCategoryDto.getName());
            tensesCategoryRepo.save(tense);

            return new ApiResponse(true, tensesCategoryDto.getId() == null ? "saved" : "Edited");


        } catch (Exception e) {

            return new ApiResponse(false, tensesCategoryDto.getId() == null ? "Not saved" : "Not Edited");
        }
    }

    public ApiResponse getTenses(UUID supperId) {
        List<TensesCategoryDto> tensesDtoList = new ArrayList<>();

        if (supperId != null) {
            tensesDtoList = tensesCategoryRepo.findByTenseCategory_Id(supperId)
                    .stream().map(this::tensesToDto)
                    .collect(Collectors.toList());
        } else {
            tensesDtoList = tensesCategoryRepo.findByTenseCategory_Id(null)
                    .stream().map(this::tensesToDto)
                    .collect(Collectors.toList());
        }
        return new ApiResponse(true, "ok", tensesDtoList);
    }


    public ApiResponse delete(UUID id) {
        try {
            tensesCategoryRepo.deleteById(id);
            return new ApiResponse(true, "Success Deleted");
        } catch (Exception e) {
            return new ApiResponse(false, "this tense is connected with other tense");
        }
    }


    public TensesCategoryDto tensesToDto(TenseCategory tense) {
        return new TensesCategoryDto(tense.getId(), tense.getName());
    }

    public ApiResponse getById(UUID id) {
        Optional<TenseCategory> optionalTense = tensesCategoryRepo.findById(id);
        if (optionalTense.isEmpty())
            return new ApiResponse(false, "Not Found");

        return new ApiResponse(true, "ok",optionalTense.get());
    }
}
