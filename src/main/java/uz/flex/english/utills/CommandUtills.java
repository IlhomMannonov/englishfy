package uz.flex.english.utills;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import uz.flex.english.exceptions.PageSizeException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandUtills {

    private static final String USERNAME_PATTERN = "^[a-zA-Z0-9]([._-](?![._-])|[a-zA-Z0-9]){3,18}[a-zA-Z0-9]$";
    private static final Pattern pattern = Pattern.compile(USERNAME_PATTERN);
    public static boolean usernameValid(final String username) {
        Matcher matcher = pattern.matcher(username);
        return matcher.matches();
    }



    public static boolean validatePageAndSize(int page,int size) throws PageSizeException {
        if (page<0){
            throw new PageSizeException("Invalid Page number. Page number must not be less than zero!");
        }else if (size<0){
            throw new PageSizeException("Invalid Page size. Page size must not be less than zero!");
        }
        else if (size>AppConstants.MAX_PAGE_SIZE){
            throw new PageSizeException("Invalid Page size. Page size must not be greater than "+AppConstants.MAX_PAGE_SIZE+"!");
        }
        return true;
    }

    public static Pageable simplePageable(int page,int size) throws PageSizeException {
        validatePageAndSize(page, size);
        return PageRequest.of(page,size);
    }

    public static Pageable descOrAscByCreatedAtPageable(int page,int size,boolean asc) throws PageSizeException {
       validatePageAndSize(page, size);
        if (asc){
            return PageRequest.of(page,size, Sort.Direction.ASC,"createdat");
        }
        return PageRequest.of(page,size, Sort.Direction.DESC,"createdat");
    }


}
