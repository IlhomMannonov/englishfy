package uz.flex.english.utills;

public interface AppConstants {
    String DEFAULT_PAGE_NUMBER="0";
    String DEFAULT_PAGE_SIZE="10";
    String DEFAULT_MESSAGE_SIZE="20";
    Integer MAX_PAGE_SIZE=20;
    String FRONT_PORT="http://localhost:3000/login";
}
